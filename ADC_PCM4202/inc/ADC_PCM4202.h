/*
 * ADC_PCM4202.h
 *
 *  Created on: 13 sept. 2017
 *      Author: alumno
 */

#ifndef ADC_PCM4202_H
#define ADC_PCM4202_H










/*****************************************************************************************************
 * 											FUNCIONES INIT
 *****************************************************************************************************/
	/**
	  * @brief Inicializa ADC PCM4202
	  * @param  None
	  * @retval None
	  */
	void ADC_PCM4202_init();










/*****************************************************************************************************
 * 											FUNCIONES LOOP
 *****************************************************************************************************/
	/**
	  * @brief Comienza recepcion de datos al ADC PCM4202 por I2S
	  * @param  None
	  * @retval None
	  */
	void ADC_PCM4202_recieve();










#endif /* ADC_PCM4202_H */
