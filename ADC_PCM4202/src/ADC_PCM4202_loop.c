/*
 * ADC_PCM4202_recieve.c
 *
 *  Created on: 13 sept. 2017
 *      Author: alumno
 */










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "ADC_PCM4202.h"
	#include "ADC_PCM4202_priv.h"

	#include "header.h"
	#include "defines.h"










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Buffers
		q31_t buffer_rx_adc_pcm4202[LEN_TX_RX*4] = {0}; // 2 (cant de canales) * 2 (porque se requiere llenar las 2 mitades)










/*****************************************************************************************************
 * 											FUNCI�N RECEPCI�N
 *****************************************************************************************************/
	void ADC_PCM4202_recieve()
	{
		  if(HAL_I2S_Receive_DMA(&hi2s2, (uint16_t *) buffer_rx_adc_pcm4202, LEN_TX_RX*4) != HAL_OK)  // 'size' = N * 2 (por ser 2 canales) * 2 (para llenar las 2 mitades del buffer)
		  {
			  Error_Handler();
		  }
	}









