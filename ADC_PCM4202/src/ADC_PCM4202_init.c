/*
 * ADC_PCM4202_init.c
 *
 *  Created on: 13 sept. 2017
 *      Author: alumno
 */










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "ADC_PCM4202.h"
	#include "ADC_PCM4202_priv.h"

	#include <stm32f4xx_hal.h>










/*****************************************************************************************************
 * 											FUNCI�N INIT
 *****************************************************************************************************/
	void ADC_PCM4202_init()
	{
		// FS2 FS1 FS0 = 000 - Single Rate with Fsckl = 768*fs
		// FS2 FS1 FS0 = 001 - Single Rate with Fsckl = 512*fs
		// FS2 FS1 FS0 = 010 - Single Rate with Fsckl = 384*fs
		// FS2 FS1 FS0 = 011 - Single Rate with Fsckl = 256*fs
		// FS2 FS1 FS0 = 100 - Dual Rate with Fsckl = 384*fs
		// FS2 FS1 FS0 = 101 - Dual Rate with Fsckl = 256*fs
		// FS2 FS1 FS0 = 110 - Quad Rate with Fsckl = 192*fs
		// FS2 FS1 FS0 = 111 - Quad Rate with Fsckl = 128*fs
			HAL_GPIO_WritePin(ADC_PCM4202_FS2_GPIO_Port, ADC_PCM4202_FS2_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(ADC_PCM4202_FS1_GPIO_Port, ADC_PCM4202_FS1_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(ADC_PCM4202_FS0_GPIO_Port, ADC_PCM4202_FS0_Pin, GPIO_PIN_SET);

		// HPFD = 0 - HPFD high-pass filter habilitado
		// HPFD = 1 - HPFD high-pass filter deshabilitado
			HAL_GPIO_WritePin(ADC_PCM4202_HPFD_GPIO_Port, ADC_PCM4202_HPFD_Pin, GPIO_PIN_SET);

		// RST - Reset activo en alto
			HAL_GPIO_WritePin(ADC_PCM4202_RST_L_GPIO_Port, ADC_PCM4202_RST_L_Pin, GPIO_PIN_SET);
	}









