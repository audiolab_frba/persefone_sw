/*
 * display_func.c
 *
 *  Created on: 7 de nov. de 2016
 *      Author: agustin
 */










/********************************************************************
 *						INCLUDES									*
 ********************************************************************/
	#include "Display_TM_ILI9341.h"
	#include "Display_TM_ILI9341_priv.h"

	#include "header.h"










/********************************************************************
 *						FUNCIONES DE PRUEBA							*
 ********************************************************************/
	void TM_ILI9341_test_pinout_toggle()
	{
		HAL_GPIO_TogglePin(LCD_CS_GPIO_Port, LCD_CS_Pin);
		HAL_GPIO_TogglePin(LCD_CS_GPIO_Port, LCD_CS_Pin);
		HAL_GPIO_TogglePin(LCD_RS_GPIO_Port, LCD_RS_Pin);
		HAL_GPIO_TogglePin(LCD_RS_GPIO_Port, LCD_RS_Pin);
		HAL_GPIO_TogglePin(LCD_WR_GPIO_Port, LCD_WR_Pin);
		HAL_GPIO_TogglePin(LCD_WR_GPIO_Port, LCD_WR_Pin);
		HAL_GPIO_TogglePin(LCD_RD_GPIO_Port, LCD_RD_Pin);
		HAL_GPIO_TogglePin(LCD_RD_GPIO_Port, LCD_RD_Pin);
		HAL_GPIO_TogglePin(LCD_RST_GPIO_Port, LCD_RST_Pin);
		HAL_GPIO_TogglePin(LCD_RST_GPIO_Port, LCD_RST_Pin);

		HAL_GPIO_TogglePin(LCD_DB1_GPIO_Port, LCD_DB1_Pin);
		HAL_GPIO_TogglePin(LCD_DB1_GPIO_Port, LCD_DB1_Pin);
		HAL_GPIO_TogglePin(LCD_DB2_GPIO_Port, LCD_DB2_Pin);
		HAL_GPIO_TogglePin(LCD_DB2_GPIO_Port, LCD_DB2_Pin);
		HAL_GPIO_TogglePin(LCD_DB3_GPIO_Port, LCD_DB3_Pin);
		HAL_GPIO_TogglePin(LCD_DB3_GPIO_Port, LCD_DB3_Pin);
		HAL_GPIO_TogglePin(LCD_DB4_GPIO_Port, LCD_DB4_Pin);
		HAL_GPIO_TogglePin(LCD_DB4_GPIO_Port, LCD_DB4_Pin);
		HAL_GPIO_TogglePin(LCD_DB5_GPIO_Port, LCD_DB5_Pin);
		HAL_GPIO_TogglePin(LCD_DB5_GPIO_Port, LCD_DB5_Pin);
		HAL_GPIO_TogglePin(LCD_DB6_GPIO_Port, LCD_DB6_Pin);
		HAL_GPIO_TogglePin(LCD_DB6_GPIO_Port, LCD_DB6_Pin);
		HAL_GPIO_TogglePin(LCD_DB7_GPIO_Port, LCD_DB7_Pin);
		HAL_GPIO_TogglePin(LCD_DB7_GPIO_Port, LCD_DB7_Pin);
		HAL_GPIO_TogglePin(LCD_DB8_GPIO_Port, LCD_DB8_Pin);
		HAL_GPIO_TogglePin(LCD_DB8_GPIO_Port, LCD_DB8_Pin);
		HAL_GPIO_TogglePin(LCD_DB10_GPIO_Port, LCD_DB10_Pin);
		HAL_GPIO_TogglePin(LCD_DB10_GPIO_Port, LCD_DB10_Pin);
		HAL_GPIO_TogglePin(LCD_DB11_GPIO_Port, LCD_DB11_Pin);
		HAL_GPIO_TogglePin(LCD_DB11_GPIO_Port, LCD_DB11_Pin);
		HAL_GPIO_TogglePin(LCD_DB12_GPIO_Port, LCD_DB12_Pin);
		HAL_GPIO_TogglePin(LCD_DB12_GPIO_Port, LCD_DB12_Pin);
		HAL_GPIO_TogglePin(LCD_DB13_GPIO_Port, LCD_DB13_Pin);
		HAL_GPIO_TogglePin(LCD_DB13_GPIO_Port, LCD_DB13_Pin);
		HAL_GPIO_TogglePin(LCD_DB14_GPIO_Port, LCD_DB14_Pin);
		HAL_GPIO_TogglePin(LCD_DB14_GPIO_Port, LCD_DB14_Pin);
		HAL_GPIO_TogglePin(LCD_DB15_GPIO_Port, LCD_DB15_Pin);
		HAL_GPIO_TogglePin(LCD_DB15_GPIO_Port, LCD_DB15_Pin);
		HAL_GPIO_TogglePin(LCD_DB16_GPIO_Port, LCD_DB16_Pin);
		HAL_GPIO_TogglePin(LCD_DB16_GPIO_Port, LCD_DB16_Pin);
		HAL_GPIO_TogglePin(LCD_DB17_GPIO_Port, LCD_DB17_Pin);
		HAL_GPIO_TogglePin(LCD_DB17_GPIO_Port, LCD_DB17_Pin);
	}

	void TM_ILI9341_test_pinout_on()
	{
		HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, 1);
		HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, 1);
		HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, 1);
		HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, 1);
		HAL_GPIO_WritePin(LCD_WR_GPIO_Port, LCD_WR_Pin, 1);
		HAL_GPIO_WritePin(LCD_WR_GPIO_Port, LCD_WR_Pin, 1);
		HAL_GPIO_WritePin(LCD_RD_GPIO_Port, LCD_RD_Pin, 1);
		HAL_GPIO_WritePin(LCD_RD_GPIO_Port, LCD_RD_Pin, 1);
		HAL_GPIO_WritePin(LCD_RST_GPIO_Port, LCD_RST_Pin, 1);
		HAL_GPIO_WritePin(LCD_RST_GPIO_Port, LCD_RST_Pin, 1);

		HAL_GPIO_WritePin(LCD_DB1_GPIO_Port, LCD_DB1_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB1_GPIO_Port, LCD_DB1_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB2_GPIO_Port, LCD_DB2_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB2_GPIO_Port, LCD_DB2_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB3_GPIO_Port, LCD_DB3_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB3_GPIO_Port, LCD_DB3_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB4_GPIO_Port, LCD_DB4_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB4_GPIO_Port, LCD_DB4_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB5_GPIO_Port, LCD_DB5_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB5_GPIO_Port, LCD_DB5_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB6_GPIO_Port, LCD_DB6_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB6_GPIO_Port, LCD_DB6_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB7_GPIO_Port, LCD_DB7_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB7_GPIO_Port, LCD_DB7_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB8_GPIO_Port, LCD_DB8_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB8_GPIO_Port, LCD_DB8_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB10_GPIO_Port, LCD_DB10_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB10_GPIO_Port, LCD_DB10_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB11_GPIO_Port, LCD_DB11_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB11_GPIO_Port, LCD_DB11_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB12_GPIO_Port, LCD_DB12_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB12_GPIO_Port, LCD_DB12_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB13_GPIO_Port, LCD_DB13_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB13_GPIO_Port, LCD_DB13_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB14_GPIO_Port, LCD_DB14_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB14_GPIO_Port, LCD_DB14_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB15_GPIO_Port, LCD_DB15_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB15_GPIO_Port, LCD_DB15_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB16_GPIO_Port, LCD_DB16_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB16_GPIO_Port, LCD_DB16_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB17_GPIO_Port, LCD_DB17_Pin, 1);
		HAL_GPIO_WritePin(LCD_DB17_GPIO_Port, LCD_DB17_Pin, 1);
	}

	void TM_ILI9341_test_pinout_off()
	{
		HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, 0);
		HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, 0);
		HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, 0);
		HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, 0);
		HAL_GPIO_WritePin(LCD_WR_GPIO_Port, LCD_WR_Pin, 0);
		HAL_GPIO_WritePin(LCD_WR_GPIO_Port, LCD_WR_Pin, 0);
		HAL_GPIO_WritePin(LCD_RD_GPIO_Port, LCD_RD_Pin, 0);
		HAL_GPIO_WritePin(LCD_RD_GPIO_Port, LCD_RD_Pin, 0);
		HAL_GPIO_WritePin(LCD_RST_GPIO_Port, LCD_RST_Pin, 0);
		HAL_GPIO_WritePin(LCD_RST_GPIO_Port, LCD_RST_Pin, 0);

		HAL_GPIO_WritePin(LCD_DB1_GPIO_Port, LCD_DB1_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB1_GPIO_Port, LCD_DB1_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB2_GPIO_Port, LCD_DB2_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB2_GPIO_Port, LCD_DB2_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB3_GPIO_Port, LCD_DB3_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB3_GPIO_Port, LCD_DB3_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB4_GPIO_Port, LCD_DB4_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB4_GPIO_Port, LCD_DB4_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB5_GPIO_Port, LCD_DB5_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB5_GPIO_Port, LCD_DB5_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB6_GPIO_Port, LCD_DB6_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB6_GPIO_Port, LCD_DB6_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB7_GPIO_Port, LCD_DB7_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB7_GPIO_Port, LCD_DB7_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB8_GPIO_Port, LCD_DB8_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB8_GPIO_Port, LCD_DB8_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB10_GPIO_Port, LCD_DB10_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB10_GPIO_Port, LCD_DB10_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB11_GPIO_Port, LCD_DB11_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB11_GPIO_Port, LCD_DB11_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB12_GPIO_Port, LCD_DB12_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB12_GPIO_Port, LCD_DB12_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB13_GPIO_Port, LCD_DB13_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB13_GPIO_Port, LCD_DB13_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB14_GPIO_Port, LCD_DB14_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB14_GPIO_Port, LCD_DB14_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB15_GPIO_Port, LCD_DB15_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB15_GPIO_Port, LCD_DB15_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB16_GPIO_Port, LCD_DB16_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB16_GPIO_Port, LCD_DB16_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB17_GPIO_Port, LCD_DB17_Pin, 0);
		HAL_GPIO_WritePin(LCD_DB17_GPIO_Port, LCD_DB17_Pin, 0);
	}










/********************************************************************
 *						PRIMITVAS (PUBLICAS)						*
 ********************************************************************/
	void TM_ILI9341_DrawPixel(uint16_t x, uint16_t y, uint16_t color)
	{
		TM_ILI9341_SetCursorPosition(x, y, x, y);
		TM_ILI9341_SendCommand(ILI9341_GRAM);
		TM_ILI9341_SendData(color);
	}

	void TM_ILI9341_Fill(uint16_t color)
	{
		if(using_tft) return;

		using_tft = 1;

		uint32_t n = 0;

		TM_ILI9341_SetCursorPosition(0, 0, ILI9341_Opts.width - 1, ILI9341_Opts.height - 1);

		TM_ILI9341_SendCommand(ILI9341_GRAM);

		for (n = 0; n < ILI9341_PIXEL; n++)
			TM_ILI9341_SendData(color);

		using_tft = 0;
	}

	void TM_ILI9341_Rotate(TM_ILI9341_Orientation orientation)
	{
		if(using_tft) return;

		using_tft = 1;

		TM_ILI9341_SendCommand(ILI9341_MAC);

		if (orientation == TM_ILI9341_Orientation_Portrait_1)
			TM_ILI9341_SendData(0x58);
		else if (orientation == TM_ILI9341_Orientation_Portrait_2)
			TM_ILI9341_SendData(0x88);
		else if (orientation == TM_ILI9341_Orientation_Landscape_1)
			TM_ILI9341_SendData(0x28);
		else if (orientation == TM_ILI9341_Orientation_Landscape_2)
			TM_ILI9341_SendData(0xE8);

		if (orientation == TM_ILI9341_Orientation_Portrait_1 || orientation == TM_ILI9341_Orientation_Portrait_2)
		{
			ILI9341_Opts.width = ILI9341_WIDTH;
			ILI9341_Opts.height = ILI9341_HEIGHT;
			ILI9341_Opts.orientation = TM_ILI9341_Portrait;
		}
		else
		{
			ILI9341_Opts.width = ILI9341_HEIGHT;
			ILI9341_Opts.height = ILI9341_WIDTH;
			ILI9341_Opts.orientation = TM_ILI9341_Landscape;
		}

		using_tft = 0;
	}

	void TM_ILI9341_Putc(uint16_t x, uint16_t y, uint8_t c, TM_FontDef_t *font, uint16_t foreground, uint16_t background)
	{
		uint32_t i = 0, b = 0, j = 0;

		// Set coordinates
		ILI9341_x = x;
		ILI9341_y = y;

		if ((ILI9341_x + font->FontWidth) > ILI9341_Opts.width)
		{
			//If at the end of a line of display, go to new line and set x to 0 position
			ILI9341_y += font->FontHeight;
			ILI9341_x = 0;
		}

		for (i = 0; i < font->FontHeight; i++)
		{
			b = font->data[(c - 32) * font->FontHeight + i];
			for (j = 0; j < font->FontWidth; j++)
			{
				if ((b << j) & 0x8000)
					TM_ILI9341_DrawPixel(ILI9341_x + j, (ILI9341_y + i), foreground);
				else
					TM_ILI9341_DrawPixel(ILI9341_x + j, (ILI9341_y + i), background);
			}
		}

		ILI9341_x += font->FontWidth;
	}

	void TM_ILI9341_Puts(uint16_t x, uint16_t y, char *str, TM_FontDef_t *font, uint16_t foreground, uint16_t background)
	{
		if(using_tft) return;

		using_tft = 1;

		// Set X and Y coordinates
		ILI9341_x = x;
		ILI9341_y = y;

		while (*str)
		{
			TM_ILI9341_Putc(ILI9341_x, ILI9341_y, *str++, font, foreground, background);
		}

		using_tft = 0;
	}

	void TM_ILI9341_GetStringSize(uint8_t *str, TM_FontDef_t *font, uint16_t *width, uint16_t *height)
	{
		uint16_t w = 0;

		*height = font->FontHeight;

		while (*str++)
			w += font->FontWidth;

		*width = w;
	}

	void TM_ILI9341_DrawLine(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color)
	{
		int16_t dx, dy, sx, sy, err, e2;


		if (x0 >= ILI9341_Opts.width)
			x0 = ILI9341_Opts.width - 1;

		if (x1 >= ILI9341_Opts.width)
			x1 = ILI9341_Opts.width - 1;

		if (y0 >= ILI9341_Opts.height)
			y0 = ILI9341_Opts.height - 1;

		if (y1 >= ILI9341_Opts.height)
			y1 = ILI9341_Opts.height - 1;

		dx = (x0 < x1) ? (x1 - x0) : (x0 - x1);
		dy = (y0 < y1) ? (y1 - y0) : (y0 - y1);
		sx = (x0 < x1) ? 1 : -1;
		sy = (y0 < y1) ? 1 : -1;
		err = ((dx > dy) ? dx : -dy) / 2;

		while (1)
		{
			TM_ILI9341_DrawPixel(x0, y0, color);

			if (x0 == x1 && y0 == y1)
				break;

			e2 = err;

			if (e2 > -dx)
			{
				err -= dy;
				x0 += sx;
			}

			if (e2 < dy)
			{
				err += dx;
				y0 += sy;
			}
		}
	}

	void TM_ILI9341_DrawRectangle(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color)
	{
		if(using_tft) return;

		using_tft = 1;

		TM_ILI9341_DrawLine(x0, y0, x1, y0, color); //Top
		TM_ILI9341_DrawLine(x0, y0, x0, y1, color);	//Left
		TM_ILI9341_DrawLine(x1, y0, x1, y1, color);	//Right
		TM_ILI9341_DrawLine(x0, y1, x1, y1, color);	//Bottom

		using_tft = 0;
	}

	void TM_ILI9341_DrawFilledRectangle(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color)
	{
		if(using_tft) return;

		using_tft = 1;

		for (; y0 < y1; y0++)
			TM_ILI9341_DrawLine(x0, y0, x1, y0, color);

		using_tft = 0;
	}

	void TM_ILI9341_DrawCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color)
	{
		if(using_tft) return;

		using_tft = 1;

		int16_t f = 1 - r;
		int16_t ddF_x = 1;
		int16_t ddF_y = -2 * r;
		int16_t x = 0;
		int16_t y = r;

		TM_ILI9341_DrawPixel(x0, y0 + r, color);
		TM_ILI9341_DrawPixel(x0, y0 - r, color);
		TM_ILI9341_DrawPixel(x0 + r, y0, color);
		TM_ILI9341_DrawPixel(x0 - r, y0, color);

		while (x < y)
		{
			if (f >= 0)
			{
				y--;
				ddF_y += 2;
				f += ddF_y;
			}

			x++;
			ddF_x += 2;
			f += ddF_x;

			TM_ILI9341_DrawPixel(x0 + x, y0 + y, color);
			TM_ILI9341_DrawPixel(x0 - x, y0 + y, color);
			TM_ILI9341_DrawPixel(x0 + x, y0 - y, color);
			TM_ILI9341_DrawPixel(x0 - x, y0 - y, color);

			TM_ILI9341_DrawPixel(x0 + y, y0 + x, color);
			TM_ILI9341_DrawPixel(x0 - y, y0 + x, color);
			TM_ILI9341_DrawPixel(x0 + y, y0 - x, color);
			TM_ILI9341_DrawPixel(x0 - y, y0 - x, color);
		}

		using_tft = 0;
	}

	void TM_ILI9341_DrawFilledCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color)
	{
		if(using_tft) return;

		using_tft = 1;

		int16_t f = 1 - r;
		int16_t ddF_x = 1;
		int16_t ddF_y = -2 * r;
		int16_t x = 0;
		int16_t y = r;

		TM_ILI9341_DrawPixel(x0, y0 + r, color);
		TM_ILI9341_DrawPixel(x0, y0 - r, color);
		TM_ILI9341_DrawPixel(x0 + r, y0, color);
		TM_ILI9341_DrawPixel(x0 - r, y0, color);
		TM_ILI9341_DrawLine(x0 - r, y0, x0 + r, y0, color);

		while (x < y)
		{
			if (f >= 0)
			{
				y--;
				ddF_y += 2;
				f += ddF_y;
			}

			x++;
			ddF_x += 2;
			f += ddF_x;

			TM_ILI9341_DrawLine(x0 - x, y0 + y, x0 + x, y0 + y, color);
			TM_ILI9341_DrawLine(x0 + x, y0 - y, x0 - x, y0 - y, color);

			TM_ILI9341_DrawLine(x0 + y, y0 + x, x0 - y, y0 + x, color);
			TM_ILI9341_DrawLine(x0 + y, y0 - x, x0 - y, y0 - x, color);
		}

		using_tft = 0;
	}









