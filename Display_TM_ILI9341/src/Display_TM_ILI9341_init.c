/*
 * Display_TM_ILI9341_init.c
 *
 *  Created on: 9 mar. 2018
 *      Author: alumno
 */










/********************************************************************
 *						INCLUDES
 ********************************************************************/
	#include "Display_TM_ILI9341.h"
	#include "Display_TM_ILI9341_priv.h"

	#include "header.h"










/********************************************************************
 *						FORWARDS
 ********************************************************************/










/********************************************************************
 *						FUNCIONES INIT
 ********************************************************************/
	void TM_ILI9341_Init(void)
	{
		// Envio de comandos de inicializacion
		TM_ILI9341_InitLCD();

		ILI9341_x = ILI9341_y = 0;
		ILI9341_Opts.width = ILI9341_WIDTH;
		ILI9341_Opts.height = ILI9341_HEIGHT;
		ILI9341_Opts.orientation = TM_ILI9341_Portrait;

		TM_ILI9341_Fill(ILI9341_COLOR_BLACK);
	}









