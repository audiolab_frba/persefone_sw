/*
 * Display_TM_ILI9341_priv.c
 *
 *  Created on: 9 mar. 2018
 *      Author: alumno
 */










/********************************************************************
 *						INCLUDES									*
 ********************************************************************/
	#include "Display_TM_ILI9341.h"
	#include "Display_TM_ILI9341_priv.h"

	#include "header.h"










/********************************************************************
*						FUNCIONES PRIVADAS
********************************************************************/
	void TM_ILI9341_Delay(volatile unsigned long int delay)
	{
		for (; delay != 0; delay--);
	}

	void TM_ILI9341_InitLCD(void)
	{
		HAL_GPIO_WritePin(LCD_RST_GPIO_Port, LCD_RST_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(LCD_RST_GPIO_Port, LCD_RST_Pin, GPIO_PIN_SET);

		TM_ILI9341_SendCommand(ILI9341_RESET);
		TM_ILI9341_Delay(2000000);

		TM_ILI9341_SendCommand(ILI9341_POWERA);
		TM_ILI9341_SendData(0x39);
		TM_ILI9341_SendData(0x2C);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendData(0x34);
		TM_ILI9341_SendData(0x02);
		TM_ILI9341_SendCommand(ILI9341_POWERB);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendData(0xC1);
		TM_ILI9341_SendData(0x30);
		TM_ILI9341_SendCommand(ILI9341_DTCA);
		TM_ILI9341_SendData(0x85);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendData(0x78);
		TM_ILI9341_SendCommand(ILI9341_DTCB);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendCommand(ILI9341_POWER_SEQ);
		TM_ILI9341_SendData(0x64);
		TM_ILI9341_SendData(0x03);
		TM_ILI9341_SendData(0x12);
		TM_ILI9341_SendData(0x81);
		TM_ILI9341_SendCommand(ILI9341_PRC);
		TM_ILI9341_SendData(0x20);
		TM_ILI9341_SendCommand(ILI9341_POWER1);
		TM_ILI9341_SendData(0x23);
		TM_ILI9341_SendCommand(ILI9341_POWER2);
		TM_ILI9341_SendData(0x10);
		TM_ILI9341_SendCommand(ILI9341_VCOM1);
		TM_ILI9341_SendData(0x3E);
		TM_ILI9341_SendData(0x28);
		TM_ILI9341_SendCommand(ILI9341_VCOM2);
		TM_ILI9341_SendData(0x86);
		TM_ILI9341_SendCommand(ILI9341_MAC);
		TM_ILI9341_SendData(0x48);
		TM_ILI9341_SendCommand(ILI9341_PIXEL_FORMAT);
		TM_ILI9341_SendData(0x55);
		TM_ILI9341_SendCommand(ILI9341_FRC);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendData(0x18);
		TM_ILI9341_SendCommand(ILI9341_DFC);
		TM_ILI9341_SendData(0x08);
		TM_ILI9341_SendData(0x82);
		TM_ILI9341_SendData(0x27);
		TM_ILI9341_SendCommand(ILI9341_3GAMMA_EN);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendCommand(ILI9341_COLUMN_ADDR);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendData(0xEF);
		TM_ILI9341_SendCommand(ILI9341_PAGE_ADDR);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendData(0x01);
		TM_ILI9341_SendData(0x3F);
		TM_ILI9341_SendCommand(ILI9341_GAMMA);
		TM_ILI9341_SendData(0x01);
		TM_ILI9341_SendCommand(ILI9341_PGAMMA);
		TM_ILI9341_SendData(0x0F);
		TM_ILI9341_SendData(0x31);
		TM_ILI9341_SendData(0x2B);
		TM_ILI9341_SendData(0x0C);
		TM_ILI9341_SendData(0x0E);
		TM_ILI9341_SendData(0x08);
		TM_ILI9341_SendData(0x4E);
		TM_ILI9341_SendData(0xF1);
		TM_ILI9341_SendData(0x37);
		TM_ILI9341_SendData(0x07);
		TM_ILI9341_SendData(0x10);
		TM_ILI9341_SendData(0x03);
		TM_ILI9341_SendData(0x0E);
		TM_ILI9341_SendData(0x09);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendCommand(ILI9341_NGAMMA);
		TM_ILI9341_SendData(0x00);
		TM_ILI9341_SendData(0x0E);
		TM_ILI9341_SendData(0x14);
		TM_ILI9341_SendData(0x03);
		TM_ILI9341_SendData(0x11);
		TM_ILI9341_SendData(0x07);
		TM_ILI9341_SendData(0x31);
		TM_ILI9341_SendData(0xC1);
		TM_ILI9341_SendData(0x48);
		TM_ILI9341_SendData(0x08);
		TM_ILI9341_SendData(0x0F);
		TM_ILI9341_SendData(0x0C);
		TM_ILI9341_SendData(0x31);
		TM_ILI9341_SendData(0x36);
		TM_ILI9341_SendData(0x0F);
		TM_ILI9341_SendCommand(ILI9341_WCD);
		TM_ILI9341_SendData(0x2C);
		TM_ILI9341_SendCommand(0xC0);
		TM_ILI9341_SendData(0x09);
		TM_ILI9341_SendCommand(ILI9341_SLEEP_OUT);

		TM_ILI9341_Delay(10000000);

		TM_ILI9341_SendCommand(ILI9341_DISPLAY_ON);
		TM_ILI9341_SendCommand(ILI9341_GRAM);
	}

	void TM_ILI9341_SendCommand(uint16_t data)
	{
		HAL_GPIO_WritePin(LCD_RD_GPIO_Port, LCD_RD_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(LCD_WR_GPIO_Port, LCD_WR_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_RESET);
		TM_Send(data);
		HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(LCD_WR_GPIO_Port, LCD_WR_Pin, GPIO_PIN_SET);
	}

	void TM_ILI9341_SendData(uint16_t data)
	{
		HAL_GPIO_WritePin(LCD_RS_GPIO_Port, LCD_RS_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(LCD_WR_GPIO_Port, LCD_WR_Pin, GPIO_PIN_RESET);
		HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_RESET);
		TM_Send(data);
		// Segun stm32
		HAL_GPIO_WritePin(LCD_CS_GPIO_Port, LCD_CS_Pin, GPIO_PIN_SET);
		HAL_GPIO_WritePin(LCD_WR_GPIO_Port, LCD_WR_Pin, GPIO_PIN_SET);
	}

	void TM_Send(uint16_t data)
	{
		HAL_GPIO_WritePin(LCD_DB1_GPIO_Port, LCD_DB1_Pin, (data >> 0) & 1);
		HAL_GPIO_WritePin(LCD_DB2_GPIO_Port, LCD_DB2_Pin, (data >> 1) & 1);
		HAL_GPIO_WritePin(LCD_DB3_GPIO_Port, LCD_DB3_Pin, (data >> 2) & 1);
		HAL_GPIO_WritePin(LCD_DB4_GPIO_Port, LCD_DB4_Pin, (data >> 3) & 1);
		HAL_GPIO_WritePin(LCD_DB5_GPIO_Port, LCD_DB5_Pin, (data >> 4) & 1);
		HAL_GPIO_WritePin(LCD_DB6_GPIO_Port, LCD_DB6_Pin, (data >> 5) & 1);
		HAL_GPIO_WritePin(LCD_DB7_GPIO_Port, LCD_DB7_Pin, (data >> 6) & 1);
		HAL_GPIO_WritePin(LCD_DB8_GPIO_Port, LCD_DB8_Pin, (data >> 7) & 1);
		HAL_GPIO_WritePin(LCD_DB10_GPIO_Port, LCD_DB10_Pin, (data >> 8) & 1);
		HAL_GPIO_WritePin(LCD_DB11_GPIO_Port, LCD_DB11_Pin, (data >> 9) & 1);
		HAL_GPIO_WritePin(LCD_DB12_GPIO_Port, LCD_DB12_Pin, (data >> 10) & 1);
		HAL_GPIO_WritePin(LCD_DB13_GPIO_Port, LCD_DB13_Pin, (data >> 11) & 1);
		HAL_GPIO_WritePin(LCD_DB14_GPIO_Port, LCD_DB14_Pin, (data >> 12) & 1);
		HAL_GPIO_WritePin(LCD_DB15_GPIO_Port, LCD_DB15_Pin, (data >> 13) & 1);
		HAL_GPIO_WritePin(LCD_DB16_GPIO_Port, LCD_DB16_Pin, (data >> 14) & 1);
		HAL_GPIO_WritePin(LCD_DB17_GPIO_Port, LCD_DB17_Pin, (data >> 15) & 1);
	}

	void TM_ILI9341_SetCursorPosition(uint16_t x1, uint16_t y1, uint16_t x2, uint16_t y2)
	{
		TM_ILI9341_SendCommand(ILI9341_COLUMN_ADDR);
		TM_ILI9341_SendData(x1 >> 8);
		TM_ILI9341_SendData(x1 & 0xFF);
		TM_ILI9341_SendData(x2 >> 8);
		TM_ILI9341_SendData(x2 & 0xFF);

		TM_ILI9341_SendCommand(ILI9341_PAGE_ADDR);
		TM_ILI9341_SendData(y1 >> 8);
		TM_ILI9341_SendData(y1 & 0xFF);
		TM_ILI9341_SendData(y2 >> 8);
		TM_ILI9341_SendData(y2 & 0xFF);
	}









