/*
 * Display_TM_ILI9341_loop.c
 *
 *  Created on: 9 mar. 2018
 *      Author: alumno
 */










/********************************************************************
 *						INCLUDES
 ********************************************************************/
	#include "Display_TM_ILI9341.h"
	#include "Display_TM_ILI9341_priv.h"

	#include "header.h"










/********************************************************************
 *						FUNCIONES									*
 ********************************************************************/
	void TM_ILI9341_Welcome_Screen(void)
	{
		//TM_ILI9341_Orientation peperota=TM_ILI9341_Landscape ;
			/* MODOS DE ORIENTACION (funcion TM_ILI9341_Rotate())
			 * 0: MAL
			 * 1: MAL
			 * 2: Vertical con pines arriba
			 * 3: Vertical con pines abajo
			 * 4: Apaisado con pines hacia la izquierda
			 * 5: Apaisado con pines hacia la derecha
			 */

		TM_ILI9341_Rotate(TM_ILI9341_Orientation_Landscape_2);
		TM_ILI9341_Fill(ILI9341_COLOR_CYAN);

		TM_ILI9341_Puts(10, 10, "Loading...", &TM_Font_16x26, ILI9341_COLOR_BLUE, ILI9341_COLOR_CYAN);

		TM_ILI9341_DrawFilledCircle(20, 50, 10, ILI9341_COLOR_YELLOW);
		TM_ILI9341_DrawFilledCircle(60, 50, 10, ILI9341_COLOR_ORANGE);
		TM_ILI9341_DrawFilledCircle(100, 50, 10, ILI9341_COLOR_RED);
		TM_ILI9341_DrawFilledCircle(140, 50, 10, ILI9341_COLOR_GREEN);
		TM_ILI9341_DrawFilledCircle(180, 50, 10, ILI9341_COLOR_BLUE);
		TM_ILI9341_DrawFilledCircle(220, 50, 10, ILI9341_COLOR_MAGENTA);
		TM_ILI9341_DrawFilledCircle(260, 50, 10, ILI9341_COLOR_GRAY);
		TM_ILI9341_DrawFilledCircle(300, 50, 10, ILI9341_COLOR_BROWN);

		TM_ILI9341_Puts(10, 90, "PERSEFONE: READY", &TM_Font_16x26, ILI9341_COLOR_BLACK, ILI9341_COLOR_CYAN);
	}

	void TM_ILI9341_MPX_scale(void)
	{
		static uint32_t first_screen = 1;
		static char str[5] = {0};
		static uint8_t sel_previo = SEL_LyR;
		static uint32_t decimal;

		if(first_screen < 250)
			first_screen++;

		if(first_screen == 250)
		{
			TM_ILI9341_Fill(ILI9341_COLOR_CYAN);

			TM_ILI9341_Puts(10, 10, "Escalado", &TM_Font_16x26, ILI9341_COLOR_BLACK, ILI9341_COLOR_CYAN);

			TM_ILI9341_DrawFilledCircle(20, 57, 5, ILI9341_COLOR_RED);

			TM_ILI9341_Puts(40, 50, "L y R: ", &TM_Font_11x18, ILI9341_COLOR_BLACK, ILI9341_COLOR_CYAN);
			TM_ILI9341_Puts(40, 90, "Piloto 19kHz: ", &TM_Font_11x18, ILI9341_COLOR_BLACK, ILI9341_COLOR_CYAN);
			TM_ILI9341_Puts(40, 130, "Piloto 38kHz: ", &TM_Font_11x18, ILI9341_COLOR_BLACK, ILI9341_COLOR_CYAN);
			TM_ILI9341_Puts(40, 170, "MPX: ", &TM_Font_11x18, ILI9341_COLOR_BLACK, ILI9341_COLOR_CYAN);

			first_screen++;
		}
		else if(first_screen > 250)
		{
			decimal = scale_l_r*100;
			sprintf(str, "0.%d", (int)decimal);
			TM_ILI9341_Puts(200, 50, str, &TM_Font_11x18, ILI9341_COLOR_BLUE, ILI9341_COLOR_CYAN);

			decimal = scale_piloto19k*100;
			sprintf(str, "0.%d", (int)decimal);
			TM_ILI9341_Puts(200, 90, str, &TM_Font_11x18, ILI9341_COLOR_BLUE, ILI9341_COLOR_CYAN);

			decimal = scale_piloto38k*100;
			sprintf(str, "0.%d", (int)decimal);
			TM_ILI9341_Puts(200, 130, str, &TM_Font_11x18, ILI9341_COLOR_BLUE, ILI9341_COLOR_CYAN);

			decimal = scale_mpx*100;
			sprintf(str, "0.%d", (int)decimal);
			TM_ILI9341_Puts(200, 170, str, &TM_Font_11x18, ILI9341_COLOR_BLUE, ILI9341_COLOR_CYAN);
		}

		if(sel_previo != sel)
		{
			switch(sel_previo)
			{
				case SEL_LyR:
					TM_ILI9341_DrawFilledCircle(20, 57, 5, ILI9341_COLOR_CYAN);
					break;
				case SEL_P19K:
					TM_ILI9341_DrawFilledCircle(20, 97, 5, ILI9341_COLOR_CYAN);
					break;
				case SEL_P38K:
					TM_ILI9341_DrawFilledCircle(20, 137, 5, ILI9341_COLOR_CYAN);
					break;
				case SEL_MPX:
					TM_ILI9341_DrawFilledCircle(20, 177, 5, ILI9341_COLOR_CYAN);
					break;
			}

			switch(sel)
			{
				case SEL_LyR:
					TM_ILI9341_DrawFilledCircle(20, 57, 5, ILI9341_COLOR_RED);
					break;
				case SEL_P19K:
					TM_ILI9341_DrawFilledCircle(20, 97, 5, ILI9341_COLOR_RED);
					break;
				case SEL_P38K:
					TM_ILI9341_DrawFilledCircle(20, 137, 5, ILI9341_COLOR_RED);
					break;
				case SEL_MPX:
					TM_ILI9341_DrawFilledCircle(20, 177, 5, ILI9341_COLOR_RED);
					break;
			}
			sel_previo = sel;
		}
	}









