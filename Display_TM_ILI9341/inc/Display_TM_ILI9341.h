/*
 * Display_TM_ILI9341.h
 *
 *  Created on: 8 mar. 2018
 *      Author: alumno
 */

#ifndef DISPLAY_TM_ILI9341_H
#define DISPLAY_TM_ILI9341_H










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "stm32f4xx_hal.h"










/*****************************************************************************************************
 * 											ESTRUCTURAS
 *****************************************************************************************************/
	// Orientation
		typedef enum
		{
			TM_ILI9341_Landscape,
			TM_ILI9341_Portrait,
			TM_ILI9341_Orientation_Portrait_1,
			TM_ILI9341_Orientation_Portrait_2,
			TM_ILI9341_Orientation_Landscape_1,
			TM_ILI9341_Orientation_Landscape_2
		} TM_ILI9341_Orientation;

	// Font struct
		typedef struct
		{
			unsigned char FontWidth;
			unsigned char FontHeight;
			const uint16_t *data;
		} TM_FontDef_t;

	// LCD options
		typedef struct
		{
			unsigned short int width;
			unsigned short int height;
			TM_ILI9341_Orientation orientation; // 1 = portrait; 0 = landscape
		} TM_ILI931_Options_t;










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Externas
		extern TM_FontDef_t TM_Font_7x10;
		extern TM_FontDef_t TM_Font_11x18;
		extern TM_FontDef_t TM_Font_16x26;










/*****************************************************************************************************
 * 											FUNCIONES INIT
 *****************************************************************************************************/
	/**
	  * @brief 	Inicializa pantalla
	  * @param  None
	  * @retval None
	  */
	void TM_ILI9341_Init(void);










/*****************************************************************************************************
 * 											FUNCIONES LOOP
 *****************************************************************************************************/
	/**
	  * @brief 	Muestra mensaje de bienvenida
	  * @param  None
	  * @retval None
	  */
	void TM_ILI9341_Welcome_Screen(void);

	/**
	  * @brief 	Pantalla de ajuste del MPX, muestra las constantes de escalado
	  * @param  None
	  * @retval None
	  */
	void TM_ILI9341_MPX_scale(void);









/*****************************************************************************************************
 * 											FUNCIONES DE PRUEBA
 *****************************************************************************************************/
	/**
	  * @brief 	Prueba pinout de la pantalla. Togglea todos los pines.
	  * @param  None
	  * @retval None
	  */
	void TM_ILI9341_test_pinout_toggle();

	/**
	  * @brief 	Prueba pinout de la pantalla. Levanta todos los pines a 1.
	  * @param  None
	  * @retval None
	  */
	void TM_ILI9341_test_pinout_on();

	/**
	  * @brief 	Prueba pinout de la pantalla. Baja todos los pines a 0.
	  * @param  None
	  * @retval None
	  */
	void TM_ILI9341_test_pinout_off();










/*****************************************************************************************************
 * 											FUNCIONES PRIMITIVAS
 *****************************************************************************************************/
	/**
	  * @brief 		Dibuja un pixel
	  * @param[in]	x		Posicion en X del pixel
	  * @param[in]	y		Posicion en Y del pixel
	  * @param[in]	color	Color del pixel
	  * @retval 	None
	  */
	void TM_ILI9341_DrawPixel (uint16_t x, uint16_t y, uint16_t color);

	/**
	  * @brief 		Pinta toda la pantalla de un color
	  * @param[in]	color	Color a pintar
	  * @retval 	None
	  */
	void TM_ILI9341_Fill (uint16_t color);

	/**
	  * @brief 		Configura la orientacion de la pantalla
	  * @param[in]	orientation	Sentido horizontal o vertical
	  * @retval 	None
	  */
	void TM_ILI9341_Rotate (TM_ILI9341_Orientation orientation);

	/**
	  * @brief 	Escribe un caracter en pantalla
	  * @param[in]	x			Posicion horizontal
	  * @param[in]	y			Posicion vertical
	  * @param[in]	c			Caracter a escribir
	  * @param[in]	*font		Fuente
	  * @param[in]	foreground	Color del caracter
	  * @param[in]	background	Color de fondo del caracter
	  * @retval None
	  */
	void TM_ILI9341_Putc (uint16_t x, uint16_t y, uint8_t c, TM_FontDef_t *font, uint16_t foreground, uint16_t background);

	/**
	  * @brief 	Escribe un string en pantalla
	  * @param[in]	x			Posicion horizontal
	  * @param[in]	y			Posicion vertical
	  * @param[in]	*str		String a escribir
	  * @param[in]	*font		Fuente
	  * @param[in]	foreground	Color del string
	  * @param[in]	background	Color de fondo del string
	  * @retval None
	  */
	void TM_ILI9341_Puts (uint16_t x, uint16_t y, char *str, TM_FontDef_t *font, uint16_t foreground, uint16_t background);

	/**
	  * @brief 	Indica cuanto tamaño ocupa un string
	  * @param[in]	*str		String
	  * @param[in]	*font		Fuente
	  * @param[out]	*width		Largo del string
	  * @param[out]	*height		Altura del string
	  * @retval None
	  */
	void TM_ILI9341_GetStringSize (uint8_t *str, TM_FontDef_t *font, uint16_t *width, uint16_t *height);

	/**
	  * @brief 	Dibuja una linea en pantalla
	  * @param[in]	x0		Coordenada en x inicial
	  * @param[in]	y0		Coordenada en y inicial
	  * @param[in]	x1		Coordenada en x final
	  * @param[in]	y1		Coordenada en y final
	  * @param[in]	color	Color de la linea
	  * @retval None
	  */
	void TM_ILI9341_DrawLine (uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color);

	/**
	  * @brief 	Dibuja un rectangulo indicandole sus vertices
	  * @param[in]	x0		Coordenada en x del vertice 1
	  * @param[in]	y0		Coordenada en y del vertice 1
	  * @param[in]	x1		Coordenada en x del vertice 2
	  * @param[in]	y1		Coordenada en y del vertice 2
	  * @param[in]	color	Color de las lineas
	  * @retval None
	  */
	void TM_ILI9341_DrawRectangle (uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color);

	/**
	  * @brief 	Dibuja un rectangulo relleno indicandole sus vertices
	  * @param[in]	x0		Coordenada en x del vertice 1
	  * @param[in]	y0		Coordenada en y del vertice 1
	  * @param[in]	x1		Coordenada en x del vertice 2
	  * @param[in]	y1		Coordenada en y del vertice 2
	  * @param[in]	color	Color de relleno
	  * @retval None
	  */
	void TM_ILI9341_DrawFilledRectangle (uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1, uint16_t color);

	/**
	  * @brief 	Dibuja un circulo
	  * @param[in]	x0		Coordenada en x del centro
	  * @param[in]	y0		Coordenada en y del centro
	  * @param[in]	r		Radio del circulo
	  * @param[in]	color	Color del circulo
	  * @retval None
	  */
	void TM_ILI9341_DrawCircle(int16_t x0, int16_t y0, int16_t r, uint16_t color);

	/**
	  * @brief 	Dibuja un circulo relleno
	  * @param[in]	x0		Coordenada en x del centro
	  * @param[in]	y0		Coordenada en y del centro
	  * @param[in]	r		Radio del circulo
	  * @param[in]	color	Color de relleno
	  * @retval None
	  */
	void TM_ILI9341_DrawFilledCircle (int16_t x0, int16_t y0, int16_t r, uint16_t color);










#endif /* DISPLAY_TM_ILI9341_H */
