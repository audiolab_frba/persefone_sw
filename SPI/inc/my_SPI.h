/*
 * SPI.h
 *
 *  Created on: 13 oct. 2017
 *      Author: alumno
 */

#ifndef INC_MY_SPI_H
#define INC_MY_SPI_H





/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "stm32f4xx_hal.h"





/*****************************************************************************************************
 * 											DEFINES
 *****************************************************************************************************/





/*****************************************************************************************************
 * 											FUNCIONES
 *****************************************************************************************************/
	/**
	  * @brief Inicializa SPI
	  * @param  None
	  * @retval None
	  */
	void SPI_Init();

	/**
	  * @brief Asigna un valor al Pote 1 SPI
	  * @param  none
	  * @retval None
	  */
	void SPI_Pote1Val(uint8_t valor);

	/**
	  * @brief Asigna un valor al Pote 2 SPI
	  * @param  none
	  * @retval None
	  */
	void SPI_Pote2Val(uint8_t valor);





#endif /* INC_MY_SPI_H */
