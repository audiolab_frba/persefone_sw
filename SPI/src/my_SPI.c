/*
 * my_SPI.c
 *
 *  Created on: 13 oct. 2017
 *      Author: alumno
 */






/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "my_SPI.h"





/*****************************************************************************************************
 * 											DEFINES
 *****************************************************************************************************/





/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	extern SPI_HandleTypeDef hspi1;





/*****************************************************************************************************
 * 											FUNCIONES
 *****************************************************************************************************/
	/**
	  * @brief Inicializa SPI
	  * @param  none
	  * @retval None
	  */
	void SPI_Init()
	{
	}

	/**
	  * @brief Asigna un valor al Pote 1 SPI
	  * @param  none
	  * @retval None
	  */
	void SPI_Pote1Val(uint8_t valor)
	{
		// Initialize GPIO CS digital pot with inst=00010001 & p1 jumper in
		// value=0 ->fc=13,263 kHz   to   value=127 ->6,631 kHz    frec_step=27Hz
		// p1 jumper out
		// value=1 ->fc=4,912 MHz   to   value=127 ->13,263 kHz    frec_step=27Hz

		uint8_t inst=0b00010001;

		HAL_GPIO_WritePin(POTE_CS1_GPIO_Port,POTE_CS1_Pin,GPIO_PIN_SET);
		HAL_SPI_Transmit(&hspi1,&inst,sizeof(inst),10);
		HAL_SPI_Transmit(&hspi1,&valor,sizeof(valor),10);
		HAL_GPIO_WritePin(POTE_CS1_GPIO_Port,POTE_CS1_Pin,GPIO_PIN_RESET);
	}

	/**
	  * @brief Asigna un valor al Pote 2 SPI
	  * @param  none
	  * @retval None
	  */
	void SPI_Pote2Val(uint8_t valor)
	{
		// Initialize GPIO CS digital pot with inst=00010001 & p1 jumper in
		// value=0 ->fc=13,263 kHz   to   value=127 ->6,631 kHz    frec_step=27Hz
		// p1 jumper out
		// value=1 ->fc=4,912 MHz   to   value=127 ->13,263 kHz    frec_step=27Hz

		uint8_t inst=0b00010001;

		HAL_GPIO_WritePin(POTE_CS2_GPIO_Port,POTE_CS2_Pin,GPIO_PIN_SET);
		HAL_SPI_Transmit(&hspi1,&inst,sizeof(inst),10);
		HAL_SPI_Transmit(&hspi1,&valor,sizeof(valor),10);
		HAL_GPIO_WritePin(POTE_CS2_GPIO_Port,POTE_CS2_Pin,GPIO_PIN_RESET);
	}
