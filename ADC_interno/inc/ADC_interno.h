/*
 * ADC_interno.h
 *
 *  Created on: 7 mar. 2018
 *      Author: alumno
 */

#ifndef ADC_INTERNO_H
#define ADC_INTERNO_H










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "stm32f4xx_hal.h"










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Externas
		extern float32_t Pote_1;
		extern float32_t Pote_2;
		extern float32_t Pote_3;










/*****************************************************************************************************
 * 											FUNCIONES
 *****************************************************************************************************/
	/**
	  * @brief 	Lee los potenciometros de la Persefone. El rango de los valores de los potenciometros es de 1 a 64
	  * @param  None
	  * @retval None
	  */
	void ADC_interno_read();










#endif /* ADC_INTERNO_H */
