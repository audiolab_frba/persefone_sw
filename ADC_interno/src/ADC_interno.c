/*
 * ADC_interno.c
 *
 *  Created on: 9 mar. 2018
 *      Author: alumno
 */










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	//#include "ADC_interno.h"

	#include "header.h"










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Globales
		float32_t Pote_1 = 0;
		float32_t Pote_2 = 0;
		float32_t Pote_3 = 0;

	// Externas
		extern ADC_HandleTypeDef hadc1;
		extern ADC_HandleTypeDef hadc2;










/*****************************************************************************************************
 * 											FUNCIONES
 *****************************************************************************************************/
	void ADC_interno_read()
	{
		HAL_ADC_Start(&hadc1);
		if (HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY) == HAL_OK) // Timeout = HAL_MAX_DELAY (bloqueante)
		{
			Pote_1 = 100.0/((uint8_t) HAL_ADC_GetValue(&hadc1) + 1); // El +1 es para asegurar valor minimo
		}
		if (HAL_ADC_PollForConversion(&hadc1, HAL_MAX_DELAY) == HAL_OK) // Timeout = HAL_MAX_DELAY (bloqueante)
		{
			Pote_2 = 100.0/((uint8_t) HAL_ADC_GetValue(&hadc1) + 1); // El +1 es para asegurar valor minimo
		}
		HAL_ADC_Stop(&hadc1);

		HAL_ADC_Start(&hadc2);
		if (HAL_ADC_PollForConversion(&hadc2, HAL_MAX_DELAY) == HAL_OK) // Timeout = HAL_MAX_DELAY (bloqueante)
		{
			Pote_3 = 100.0/((uint8_t) HAL_ADC_GetValue(&hadc2) + 1); // El +1 es para asegurar valor minimo
		}
		HAL_ADC_Stop(&hadc2);
	}









