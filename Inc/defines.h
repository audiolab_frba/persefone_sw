/**
 *  Defines for your entire project at one place
 *
 *	@author 	Tilen Majerle
 *	@email		tilen@majerle.eu
 *	@website	http://stm32f4-discovery.com
 *	@version 	v1.0
 *	@ide		Keil uVision 5
 *	@license	GNU GPL v3
 *
 * |----------------------------------------------------------------------
 * | Copyright (C) Tilen Majerle, 2014
 * |
 * | This program is free software: you can redistribute it and/or modify
 * | it under the terms of the GNU General Public License as published by
 * | the Free Software Foundation, either version 3 of the License, or
 * | any later version.
 * |
 * | This program is distributed in the hope that it will be useful,
 * | but WITHOUT ANY WARRANTY; without even the implied warranty of
 * | MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * | GNU General Public License for more details.
 * |
 * | You should have received a copy of the GNU General Public License
 * | along with this program.  If not, see <http://www.gnu.org/licenses/>.
 * |----------------------------------------------------------------------
 */
#ifndef TM_DEFINES_H
#define TM_DEFINES_H

/* Put your global defines for all libraries here used in your project */









/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include <stm32f4xx_hal.h>
	#include "my_arm_math.h"










/*****************************************************************************************************
 * 											DEFINES
 *****************************************************************************************************/
	#define LEN_TX_RX		1024










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Globales
		// Buffer 1 de LEN_TX_RX elementos de 32 bits cada uno.
			extern uint32_t*	buffer_aux1_u32;
			extern q15_t*		buffer_aux1_q15;
			extern q31_t*		buffer_aux1_q31;
			extern float32_t*	buffer_aux1_f32;

		// Buffer 2 de LEN_TX_RX elementos de 32 bits cada uno.
			extern uint32_t*	buffer_aux2_u32;
			extern q15_t*		buffer_aux2_q15;
			extern q31_t*		buffer_aux2_q31;
			extern float32_t*	buffer_aux2_f32;

		// Variables de escalado de la trama MPX
			extern float32_t scale_l_r;
			extern float32_t scale_piloto19k;
			extern float32_t scale_piloto38k;
			extern float32_t scale_mpx;










#endif
