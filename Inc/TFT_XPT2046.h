#ifndef __TFT_XPT2046_H__
#define __TFT_XPT2046_H__

#include "header.h"



//
//#define XPT2046_CS 		LATDbits.LATD5 // XPT2046 Chip select
//#define XPT2046_IRQ 	PORTDbits.RD11 // XPT2046 IRQ
//
//#define XPT2046_CS_DIR 	TRISDbits.TRISD5 // XPT2046 Chip select Direction
//#define XPT2046_IRQ_DIR TRISDbits.TRISD11 // XPT2046 IRQ Direction



uint32_t sendGetXPT2046 (uint8_t byte); // SPI send to XPT2046
uint32_t XPT_2046_GET_X(); // Get X coordinate
uint32_t XPT_2046_GET_Y(); // Get Y coordinate
void XPT_2046_Init(); // Initialise XPT2046



//
///***************************************************************************************************
////不使用触摸或者模块本身不带触摸，则可不连接
////MO(MISO)	接PC2	//SPI总线输出
////MI(MOSI)	接PC3	//SPI总线输入
////PEN		接PC1	//触摸屏中断信号
////TCS		接PC13	//触摸IC片选
////CLK		接PC0	//SPI总线时钟
//**************************************************************************************************/
//
//#define TP_PRES_DOWN 0x80  //触屏被按下
//#define TP_CATH_PRES 0x40  //有按键按下了
//
////触摸屏控制器
//typedef struct
//{
//	uint8_t (*init)(void);			//初始化触摸屏控制器
//	uint8_t (*scan)(uint8_t);				//扫描触摸屏.0,屏幕扫描;1,物理坐标;
//	void (*adjust)(void);		//触摸屏校准
//	uint16_t x0;						//原始坐标(第一次按下时的坐标)
//	uint16_t y0;
//	uint16_t x; 						//当前坐标(此次扫描时,触屏的坐标)
//	uint16_t y;
//	uint8_t  sta;					//笔的状态
//								//b7:按下1/松开0;
//	                            //b6:0,没有按键按下;1,有按键按下.
//////////////////////////触摸屏校准参数/////////////////////////
//	float xfac;
//	float yfac;
//	short xoff;
//	short yoff;
////新增的参数,当触摸屏的左右上下完全颠倒时需要用到.
////touchtype=0的时候,适合左右为X坐标,上下为Y坐标的TP.
////touchtype=1的时候,适合左右为Y坐标,上下为X坐标的TP.
//	uint8_t touchtype;
//}_m_tp_dev;
//
//extern _m_tp_dev tp_dev;	 	//触屏控制器在touch.c里面定义
//
//
//#define PEN  PCin(1)   //PC1  INT
//#define DOUT PCin(2)   //PC2  MISO
//#define TDIN PCout(3)  //PC3  MOSI
//#define TCLK PCout(0)  //PC0  SCLK
//#define TCS  PCout(13) //PC13 CS
//
//
//void TP_Write_Byte(uint8_t num);						//向控制芯片写入一个数据
//uint16_t TP_Read_AD(uint8_t CMD);							//读取AD转换值
//uint16_t TP_Read_XOY(uint8_t xy);							//带滤波的坐标读取(X/Y)
//uint8_t TP_Read_XY(uint16_t *x,uint16_t *y);					//双方向读取(X+Y)
//uint8_t TP_Read_XY2(uint16_t *x,uint16_t *y);					//带加强滤波的双方向坐标读取
//void TP_Drow_Touch_Point(uint16_t x,uint16_t y,uint16_t color);//画一个坐标校准点
//void TP_Draw_Big_Point(uint16_t x,uint16_t y,uint16_t color);	//画一个大点
//uint8_t TP_Scan(uint8_t tp);								//扫描
//void TP_Save_Adjdata(void);						//保存校准参数
//uint8_t TP_Get_Adjdata(void);						//读取校准参数
//void TP_Adjust(void);							//触摸屏校准
//uint8_t TP_Init(void);								//初始化
//
//void TP_Adj_Info_Show(uint16_t x0,uint16_t y0,uint16_t x1,uint16_t y1,uint16_t x2,uint16_t y2,uint16_t x3,uint16_t y3,uint16_t fac);//显示校准信息
 		  








#endif





