/*
 * header.h
 *
 *  Created on: 13 sept. 2017
 *      Author: alumno
 */

#ifndef HEADER_H
#define HEADER_H










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include <stm32f4xx_hal.h>

	#include "my_arm_math.h"
	#include "my_I2S.h"
	#include "my_SPI.h"
	#include "ADC_PCM4202.h"
	#include "DAC_PCM1781.h"
	#include "ADC_interno.h"
	#include "Display_TM_ILI9341.h"
	#include "Display_TM_ILI9341_priv.h"
	#include "TFT_XPT2046.h"
	#include "mpx.h"
	#include "pulsadores.h"
	#include "led.h"
	#include "ADC_interno.h"
	#include "LPFNormaFM.h"
	#include "HPF.h"










#endif /* HEADER_H */
