/*
 * mpx.h
 *
 *  Created on: 7 mar. 2018
 *      Author: alumno
 */

#ifndef MPX_H
#define MPX_H










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "header.h"










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Externas
	// Internas










/*****************************************************************************************************
 * 											FUNCIONES
 *****************************************************************************************************/
	/**
	  * @brief 	None
	  * @param  None
	  * @retval None
	  */
	void mpx_loop();

	/**
	  * @brief 	Funcion para medir tiempos de procesamiento con distintos GPIOs.
	  * @param  None
	  * @retval None
	  */
	void mpx_measure_times();








/*****************************************************************************************************
 * 											INTERRUPCIONES
 *****************************************************************************************************/
	/**
	  * @brief 	None
	  * @param  None
	  * @retval None
	  */













#endif /* MPX_H_ */
