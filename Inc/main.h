/**
  ******************************************************************************
  * File Name          : main.h
  * Description        : This file contains the common defines of the application
  ******************************************************************************
  ** This notice applies to any and all portions of this file
  * that are not between comment pairs USER CODE BEGIN and
  * USER CODE END. Other portions of this file, whether 
  * inserted by the user or by software development tools
  * are owned by their respective copyright owners.
  *
  * COPYRIGHT(c) 2018 STMicroelectronics
  *
  * Redistribution and use in source and binary forms, with or without modification,
  * are permitted provided that the following conditions are met:
  *   1. Redistributions of source code must retain the above copyright notice,
  *      this list of conditions and the following disclaimer.
  *   2. Redistributions in binary form must reproduce the above copyright notice,
  *      this list of conditions and the following disclaimer in the documentation
  *      and/or other materials provided with the distribution.
  *   3. Neither the name of STMicroelectronics nor the names of its contributors
  *      may be used to endorse or promote products derived from this software
  *      without specific prior written permission.
  *
  * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
  * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
  * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
  * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
  * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
  * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
  * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
  * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
  * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
  * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
  *
  ******************************************************************************
  */
/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __MAIN_H
#define __MAIN_H
  /* Includes ------------------------------------------------------------------*/

/* USER CODE BEGIN Includes */


/* USER CODE END Includes */

/* Private define ------------------------------------------------------------*/

#define DAC_PCM1781_MUTE_Pin GPIO_PIN_4
#define DAC_PCM1781_MUTE_GPIO_Port GPIOE
#define DAC_PCM1781_DEMP1_Pin GPIO_PIN_5
#define DAC_PCM1781_DEMP1_GPIO_Port GPIOE
#define DAC_PCM1781_DEMP0_Pin GPIO_PIN_6
#define DAC_PCM1781_DEMP0_GPIO_Port GPIOE
#define DAC_PCM1781_ZEROA_Pin GPIO_PIN_13
#define DAC_PCM1781_ZEROA_GPIO_Port GPIOC
#define ADC_PCM4202_RST_L_Pin GPIO_PIN_0
#define ADC_PCM4202_RST_L_GPIO_Port GPIOF
#define ADC_PCM4202_HPFD_Pin GPIO_PIN_1
#define ADC_PCM4202_HPFD_GPIO_Port GPIOF
#define ADC_PCM4202_FS2_Pin GPIO_PIN_2
#define ADC_PCM4202_FS2_GPIO_Port GPIOF
#define ADC_PCM4202_FS1_Pin GPIO_PIN_3
#define ADC_PCM4202_FS1_GPIO_Port GPIOF
#define ADC_PCM4202_FS0_Pin GPIO_PIN_4
#define ADC_PCM4202_FS0_GPIO_Port GPIOF
#define ADC_PCM4202_CLIPR_Pin GPIO_PIN_5
#define ADC_PCM4202_CLIPR_GPIO_Port GPIOF
#define ADC_PCM4202_CLIPL_Pin GPIO_PIN_6
#define ADC_PCM4202_CLIPL_GPIO_Port GPIOF
#define ADC_PCM4202_DATA_Pin GPIO_PIN_3
#define ADC_PCM4202_DATA_GPIO_Port GPIOC
#define POTE_1_Pin GPIO_PIN_0
#define POTE_1_GPIO_Port GPIOA
#define POTE_2_Pin GPIO_PIN_1
#define POTE_2_GPIO_Port GPIOA
#define DAC_PCM1781_LRCK_Pin GPIO_PIN_4
#define DAC_PCM1781_LRCK_GPIO_Port GPIOA
#define SPI_CLK_Pin GPIO_PIN_5
#define SPI_CLK_GPIO_Port GPIOA
#define SPI_MISO_Pin GPIO_PIN_6
#define SPI_MISO_GPIO_Port GPIOA
#define SPI_MOSI_Pin GPIO_PIN_7
#define SPI_MOSI_GPIO_Port GPIOA
#define POTE_CS1_Pin GPIO_PIN_4
#define POTE_CS1_GPIO_Port GPIOC
#define POTE_CS2_Pin GPIO_PIN_5
#define POTE_CS2_GPIO_Port GPIOC
#define POTE_3_Pin GPIO_PIN_0
#define POTE_3_GPIO_Port GPIOB
#define LED_B_Pin GPIO_PIN_1
#define LED_B_GPIO_Port GPIOB
#define LED_R_Pin GPIO_PIN_2
#define LED_R_GPIO_Port GPIOB
#define LED_G_Pin GPIO_PIN_11
#define LED_G_GPIO_Port GPIOF
#define SW1_Pin GPIO_PIN_14
#define SW1_GPIO_Port GPIOF
#define SW2_Pin GPIO_PIN_15
#define SW2_GPIO_Port GPIOF
#define SW4_Pin GPIO_PIN_0
#define SW4_GPIO_Port GPIOG
#define SW3_Pin GPIO_PIN_1
#define SW3_GPIO_Port GPIOG
#define SPI_CS_Pin GPIO_PIN_15
#define SPI_CS_GPIO_Port GPIOE
#define ADC_PCM4202_BCK_Pin GPIO_PIN_10
#define ADC_PCM4202_BCK_GPIO_Port GPIOB
#define T_CS_Pin GPIO_PIN_12
#define T_CS_GPIO_Port GPIOB
#define T_PEN_Pin GPIO_PIN_13
#define T_PEN_GPIO_Port GPIOB
#define EXP0_Pin GPIO_PIN_14
#define EXP0_GPIO_Port GPIOB
#define EXP1_Pin GPIO_PIN_15
#define EXP1_GPIO_Port GPIOB
#define EXP2_Pin GPIO_PIN_8
#define EXP2_GPIO_Port GPIOD
#define EXP3_Pin GPIO_PIN_9
#define EXP3_GPIO_Port GPIOD
#define LCD_DB17_Pin GPIO_PIN_11
#define LCD_DB17_GPIO_Port GPIOD
#define EXP4_Pin GPIO_PIN_12
#define EXP4_GPIO_Port GPIOD
#define EXP5_Pin GPIO_PIN_13
#define EXP5_GPIO_Port GPIOD
#define LCD_DB16_Pin GPIO_PIN_14
#define LCD_DB16_GPIO_Port GPIOD
#define LCD_DB15_Pin GPIO_PIN_15
#define LCD_DB15_GPIO_Port GPIOD
#define EXP6_Pin GPIO_PIN_2
#define EXP6_GPIO_Port GPIOG
#define EXP7_Pin GPIO_PIN_3
#define EXP7_GPIO_Port GPIOG
#define LCD_DB14_Pin GPIO_PIN_4
#define LCD_DB14_GPIO_Port GPIOG
#define LCD_DB13_Pin GPIO_PIN_5
#define LCD_DB13_GPIO_Port GPIOG
#define LCD_DB12_Pin GPIO_PIN_6
#define LCD_DB12_GPIO_Port GPIOG
#define LCD_DB11_Pin GPIO_PIN_7
#define LCD_DB11_GPIO_Port GPIOG
#define LCD_DB10_Pin GPIO_PIN_8
#define LCD_DB10_GPIO_Port GPIOG
#define ADC_PCM4202_SCKI_Pin GPIO_PIN_6
#define ADC_PCM4202_SCKI_GPIO_Port GPIOC
#define DAC_PCM1781_SCK_Pin GPIO_PIN_7
#define DAC_PCM1781_SCK_GPIO_Port GPIOC
#define LCD_DB8_Pin GPIO_PIN_8
#define LCD_DB8_GPIO_Port GPIOC
#define DAC_PCM1781_BCK_Pin GPIO_PIN_10
#define DAC_PCM1781_BCK_GPIO_Port GPIOC
#define LCD_DB7_Pin GPIO_PIN_11
#define LCD_DB7_GPIO_Port GPIOC
#define LCD_DB6_Pin GPIO_PIN_12
#define LCD_DB6_GPIO_Port GPIOC
#define LCD_DB5_Pin GPIO_PIN_2
#define LCD_DB5_GPIO_Port GPIOD
#define LCD_DB4_Pin GPIO_PIN_3
#define LCD_DB4_GPIO_Port GPIOD
#define EXP11_Pin GPIO_PIN_4
#define EXP11_GPIO_Port GPIOD
#define EXP12_Pin GPIO_PIN_7
#define EXP12_GPIO_Port GPIOD
#define LCD_DB3_Pin GPIO_PIN_9
#define LCD_DB3_GPIO_Port GPIOG
#define LCD_DB2_Pin GPIO_PIN_10
#define LCD_DB2_GPIO_Port GPIOG
#define EXP13_Pin GPIO_PIN_11
#define EXP13_GPIO_Port GPIOG
#define EXP14_Pin GPIO_PIN_12
#define EXP14_GPIO_Port GPIOG
#define LCD_DB1_Pin GPIO_PIN_13
#define LCD_DB1_GPIO_Port GPIOG
#define LCD_RST_Pin GPIO_PIN_14
#define LCD_RST_GPIO_Port GPIOG
#define LCD_RD_Pin GPIO_PIN_15
#define LCD_RD_GPIO_Port GPIOG
#define LCD_WR_Pin GPIO_PIN_4
#define LCD_WR_GPIO_Port GPIOB
#define DAC_PCM1781_DATA_Pin GPIO_PIN_5
#define DAC_PCM1781_DATA_GPIO_Port GPIOB
#define EXP15_Pin GPIO_PIN_8
#define EXP15_GPIO_Port GPIOB
#define ADC_PCM4202_LRBK_Pin GPIO_PIN_9
#define ADC_PCM4202_LRBK_GPIO_Port GPIOB
#define LCD_RS_Pin GPIO_PIN_0
#define LCD_RS_GPIO_Port GPIOE
#define LCD_CS_Pin GPIO_PIN_1
#define LCD_CS_GPIO_Port GPIOE

/* USER CODE BEGIN Private defines */

/* USER CODE END Private defines */

void _Error_Handler(char *, int);

#define Error_Handler() _Error_Handler(__FILE__, __LINE__)

/**
  * @}
  */ 

/**
  * @}
*/ 

#endif /* __MAIN_H */
/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
