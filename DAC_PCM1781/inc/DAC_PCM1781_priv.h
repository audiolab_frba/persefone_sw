/*
 * DAC_PCM1781_priv.h
 *
 *  Created on: 9 mar. 2018
 *      Author: alumno
 */

#ifndef DAC_PCM1781_PRIV_H
#define DAC_PCM1781_PRIV_H










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "stm32f4xx_hal.h"

	#include "defines.h"










/*****************************************************************************************************
 * 											ERRORES
 *****************************************************************************************************/
	#ifndef LEN_TX_RX
		#error El usuario debe crear el define LEN_TX_RX en su propio "defines.h". LEN_TX_RX refiere a la cantidad de muestras que tendr� el buffer del ADC/DAC.
	#endif










/*****************************************************************************************************
 * 											DEFINES
 *****************************************************************************************************/
	#define DAC_PCM1781_BUFFER_LENGTH	LEN_TX_RX










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Externas
		extern I2S_HandleTypeDef hi2s3;










#endif /* DAC_PCM1781_PRIV_H */
