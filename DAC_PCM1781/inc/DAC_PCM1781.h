/*
 * DAC_PCM1781.h
 *
 *  Created on: 13 sept. 2017
 *      Author: alumno
 */

#ifndef DAC_PCM1781_H
#define DAC_PCM1781_H










/*****************************************************************************************************
 * 											FUNCIONES INIT
 *****************************************************************************************************/
	/**
	  * @brief Inicializa DAC PCM1781
	  * @param  None
	  * @retval None
	  */
	void DAC_PCM1781_init();










/*****************************************************************************************************
 * 											FUNCIONES LOOP
 *****************************************************************************************************/
	/**
	  * @brief Comienza transmision de datos del DAC PCM1781 por I2S
	  * @param  None
	  * @retval None
	  */
	void DAC_PCM1781_transmit();










#endif /* DAC_PCM1781_H */
