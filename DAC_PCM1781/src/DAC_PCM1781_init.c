/*
 * DAC_PCM1781_init.c
 *
 *  Created on: 13 sept. 2017
 *      Author: alumno
 */





/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "DAC_PCM1781.h"
	#include "DAC_PCM1781_priv.h"

	#include "header.h"





/*****************************************************************************************************
 * 											FUNCI�N INIT
 *****************************************************************************************************/
	void DAC_PCM1781_init()
	{
		  // DEMP1 DEMP0 = 00 - de-emphasis OFF
		  // DEMP1 DEMP0 = 01 - de-emphasis 48 KHz ON
		  // DEMP1 DEMP0 = 10 - de-emphasis 44.1 KHz ON
		  // DEMP1 DEMP0 = 11 - de-emphasis 32 KHz ON
			  HAL_GPIO_WritePin(DAC_PCM1781_DEMP1_GPIO_Port, DAC_PCM1781_DEMP1_Pin, GPIO_PIN_RESET);
			  HAL_GPIO_WritePin(DAC_PCM1781_DEMP0_GPIO_Port, DAC_PCM1781_DEMP0_Pin, GPIO_PIN_RESET);

		  // MUTE = 0 - Mute OFF
		  // MUTE = 1 - Mute ON
			  HAL_GPIO_WritePin(DAC_PCM1781_MUTE_GPIO_Port, DAC_PCM1781_MUTE_Pin, GPIO_PIN_RESET);
	}
