/*
 * ADC_PCM4202_recieve.c
 *
 *  Created on: 13 sept. 2017
 *      Author: alumno
 */










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "DAC_PCM1781.h"
	#include "DAC_PCM1781_priv.h"

	#include "header.h"
	#include "defines.h"










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Buffers
		q31_t buffer_tx_dac_pcm1781[LEN_TX_RX*4] = {0}; // 2 (cant de canales) * 2 (porque se requiere llenar las 2 mitades)










/*****************************************************************************************************
 * 											RECEPCION
 *****************************************************************************************************/
	void DAC_PCM1781_transmit()
	{
		  if(HAL_I2S_Transmit_DMA(&hi2s3, (uint16_t *) buffer_tx_dac_pcm1781, LEN_TX_RX*4) != HAL_OK) // 'size' = N * 2 (por ser 2 canales) * 2 (para llenar las 2 mitades del buffer)
			  Error_Handler();
	}
