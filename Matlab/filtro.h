/* Generated with MatlabSOS2CMSIS
Phil Birch, University of Sussex, 2017*/
#define NUM_SECTIONS 5
float32_t pCoeffs[]={ 8.67651761E-01, -1.73530352E+00, 8.67651761E-01, 1.61409533E+00, -8.56511772E-01,
 7.63465226E-01, -1.52693045E+00, 7.63465226E-01, 1.42027676E+00, -6.33584082E-01,
 6.92708433E-01, -1.38541687E+00, 6.92708433E-01, 1.28864765E+00, -4.82186019E-01,
 6.49007559E-01, -1.29801512E+00, 6.49007559E-01, 1.20735085E+00, -3.88679504E-01,
 6.28173113E-01, -1.25634623E+00, 6.28173113E-01, 1.16859245E+00, -3.44100058E-01,};

/*Example usage:
#include "filtro.h"
float32_t pState[NUM_SECTIONS*4]={0};
arm_biquad_casd_df1_inst_f32 S;

In the your main function init the filter
arm_biquad_cascade_df1_init_f32(&S,NUM_SECTIONS,pCoeffs,pState);
To run the filter:
arm_biquad_cascade_df1_f32(&S,pSrc,pDest,BUFFER_SIZE);
See CMSIS doc for varible descriptions
*/