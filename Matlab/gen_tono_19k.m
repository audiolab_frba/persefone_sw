close all;
clear all;
clc;


fs = 114.00e3;
ts= 1/fs;
f = 19000;
n = 1:12;

t = ts * n;
w = f * 2  * pi;

out = sin(t*w);

vpa(out')