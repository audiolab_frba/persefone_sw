/*
 * I2S_IRQ.c
 *
 *  Created on: 13 sept. 2017
 *      Author: alumno
 */










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	//#include "my_I2S.h"
	#include "my_I2S_priv.h"

	#include "defines.h"
	#include "header.h"











/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Globales
		// RX Left
			float32_t *buffer_rx_left = NULL;

		// RX Right
			float32_t *buffer_rx_right = NULL;

		// TX Left
			float32_t *buffer_tx_left = NULL;

		// TX Right
			float32_t *buffer_tx_right = NULL;

		// Flags
			uint8_t transmit_ready = HAL_BUSY;
			uint8_t recieve_ready = HAL_BUSY;

	// Estaticas
		// RX Left
			static q31_t buffer_rx_left_q31[LEN_TX_RX] = {0};
		// RX Right
			static q31_t buffer_rx_right_q31[LEN_TX_RX] = {0};
		// TX Left
			static q31_t buffer_tx_left_q31[LEN_TX_RX] = {0};
		// TX Right
			static q31_t buffer_tx_right_q31[LEN_TX_RX] = {0};










/*****************************************************************************************************
 * 											FUNCIONES
 *****************************************************************************************************/
	void I2S_Read()
	{
		buffer_rx_left  = (float32_t *) buffer_rx_left_q31;
		buffer_rx_right = (float32_t *) buffer_rx_right_q31;
		buffer_tx_left  = (float32_t *) buffer_tx_left_q31;
		buffer_tx_right = (float32_t *) buffer_tx_right_q31;

		arm_q31_to_float(buffer_rx_left_q31, buffer_rx_left, LEN_TX_RX);
		arm_q31_to_float(buffer_rx_right_q31, buffer_rx_right, LEN_TX_RX);
	}

	void I2S_Write()
	{
		arm_float_to_q31(buffer_tx_left, buffer_tx_left_q31, LEN_TX_RX);
		arm_float_to_q31(buffer_tx_right, buffer_tx_right_q31, LEN_TX_RX);

		buffer_rx_left  = NULL;
		buffer_rx_right = NULL;
		buffer_tx_left  = NULL;
		buffer_tx_right = NULL;

		transmit_ready = HAL_BUSY;
	}

	uint32_t I2S_get_fs()
	{
		uint8_t i2s_div = hi2s2.Instance->I2SPR & 0xFF;
		uint8_t i2s_odd = (hi2s2.Instance->I2SPR >> 8 ) & 0x01;

		uint32_t fs = 175111111 / ((32*2)*((2*i2s_div)+i2s_odd)*4);

		return fs;
	}

	void I2S_init_clk()
	{
		/*	Calculo para configurar la frecuencia del periferico I2S
		 *	FS = I2SxCLK / [(32*2)*((2*I2SDIV)+ODD)*4)] when the channel frame is 32-bit wide
		 * 	Para obtener el piloto de 19kHz lo mas preciso posible se selecciono una FS = 6 * 19kHz = 114 kHz
		 * 	Fijando FS, la frecuencia I2SxCLK mas adecuada es 175,111 MHz, para obtenerla:
		 * 		ODD = 0
		 * 		I2SDIV = 3
		 * 	Con
		 * 	I2SDIV = SPI_I2SPR[7:0]
	 	 *	I2SODD = SPI_I2SPR[8]
		 */
		uint32_t i2sdiv = 3;
		uint32_t i2sodd = (0 << 8);

		/* Write to SPIx I2SPR register the computed value */
		hi2s2.Instance->I2SPR = (uint32_t)((uint32_t)i2sdiv | (uint32_t)(i2sodd | (uint32_t)hi2s2.Init.MCLKOutput));
		hi2s3.Instance->I2SPR = (uint32_t)((uint32_t)i2sdiv | (uint32_t)(i2sodd | (uint32_t)hi2s3.Init.MCLKOutput));

		hi2s2.Init.AudioFreq = 114000U;
		hi2s3.Init.AudioFreq = 114000U;
	}








/*****************************************************************************************************
 * 											INTERRUPCIONES
 *****************************************************************************************************/
	 void HAL_I2S_TxHalfCpltCallback(I2S_HandleTypeDef *hi2s)
	{
		 // NOTA: la funcion arm_copy...() (o for con i--) anda mal en estas interrupciones

		/*
		 * Definiciones:
		 *
		 * buffer_rx_left_q31 =  &(buffer_rx_adc_pcm4202[0])
		 * buffer_rx_right_q31 = &(buffer_rx_adc_pcm4202[LEN_TX_RX + 0])
		 *
		 *
		 * Los datos se reciben/transmiten con el siguiente formato:
		 *
		 * buffer_rx_left_q31[0] = L_L H_L
		 * buffer_rx_left_q31[1] = L_R H_R
		 * buffer_rx_left_q31[2] = L_L H_L
		 * buffer_rx_left_q31[3] = L_R H_R
		 * ...
		 * buffer_rx_right_q31[0] = L_L H_L
		 * buffer_rx_right_q31[1] = L_R H_R
		 * buffer_rx_right_q31[2] = L_L H_L
		 * buffer_rx_right_q31[3] = L_R H_R
		 *
		 *
		 * Se busca obtener el siguiente formato:
		 *
		 * buffer_rx_left_q31[0]  = H_L L_L
		 * buffer_rx_left_q31[1]  = H_L L_L
		 * ...
		 * buffer_rx_right_q31[0] = H_R L_R
		 * buffer_rx_right_q31[1] = H_R L_R
		 */

		q31_t h_l = 0; // High left
		q31_t l_l = 0; // Low  left
		q31_t h_r = 0; // High right
		q31_t l_r = 0; // Low  right

		HAL_GPIO_TogglePin(EXP6_GPIO_Port, EXP6_Pin);

		// buffer_tx_dac_pcm1781[i] = buffer_rx_adc_pcm4202[i]; // Loopback

		for(int i=0, j=0; i<LEN_TX_RX*2 ; i+=2, j++)
		{
			// RX
				// Posicion par LEFT: solo volteo half-words de LEFT
					h_l = buffer_rx_adc_pcm4202[i] 		&  0x0000FFFF;
					l_l = (buffer_rx_adc_pcm4202[i] >> 16) &  0x0000FFFF;

					buffer_rx_left_q31[j]  = (h_l<<16) | l_l;

				// Posicion impar RIGHT: solo volteo half-words de RIGHT y las copio en el buffer auxiliar 1
					h_r = buffer_rx_adc_pcm4202[i+1]		  &  0x0000FFFF;
					l_r = (buffer_rx_adc_pcm4202[i+1] >> 16) &  0x0000FFFF;

					buffer_rx_right_q31[j] = (h_r<<16) | l_r;

			// TX
				// Posicion par LEFT: solo volteo half-words de LEFT
					h_l = buffer_tx_left_q31[j] 		&  0x0000FFFF;
					l_l = (buffer_tx_left_q31[j] >> 16) &  0x0000FFFF;

					buffer_tx_dac_pcm1781[i]  = (h_l<<16) | l_l;

					// Posicion impar RIGHT: solo volteo half-words de RIGHT y las copio en el buffer auxiliar 2
					h_r = buffer_tx_right_q31[j] 		  &  0x0000FFFF;
					l_r = (buffer_tx_right_q31[j] >> 16) &  0x0000FFFF;

					buffer_tx_dac_pcm1781[i+1] = (h_r<<16) | l_r;
		}

		HAL_GPIO_TogglePin(EXP6_GPIO_Port, EXP6_Pin);

		transmit_ready = HAL_OK;
	}

	 void HAL_I2S_TxCpltCallback(I2S_HandleTypeDef *hi2s)
	{
		 // NOTA: la funcion arm_copy...() (o for con i--) anda mal en estas interrupciones

		/*
		 * Definiciones:
		 *
		 * buffer_rx_left_q31 =  &(buffer_rx_adc_pcm4202[LEN_TX_RX*2 + 0])
		 * buffer_rx_right_q31 = &(buffer_rx_adc_pcm4202[LEN_TX_RX*3 + 0])
		 *
		 *
		 * Los datos se reciben/transmiten con el siguiente formato:
		 *
		 * buffer_rx_left_q31[0] = L_L H_L
		 * buffer_rx_left_q31[1] = L_R H_R
		 * buffer_rx_left_q31[2] = L_L H_L
		 * buffer_rx_left_q31[3] = L_R H_R
		 * ...
		 * buffer_rx_right_q31[0] = L_L H_L
		 * buffer_rx_right_q31[1] = L_R H_R
		 * buffer_rx_right_q31[2] = L_L H_L
		 * buffer_rx_right_q31[3] = L_R H_R
		 *
		 *
		 * Se busca obtener el siguiente formato:
		 *
		 * buffer_rx_left_q31[0]  = H_L L_L
		 * buffer_rx_left_q31[1]  = H_L L_L
		 * ...
		 * buffer_rx_right_q31[0] = H_R L_R
		 * buffer_rx_right_q31[1] = H_R L_R
		 */


		q31_t h_l; // High left
		q31_t l_l; // Low  left
		q31_t h_r; // High right
		q31_t l_r; // Low  right


		HAL_GPIO_TogglePin(EXP6_GPIO_Port, EXP6_Pin);


		// buffer_tx_dac_pcm1781[i] = buffer_rx_adc_pcm4202[i]; // Loopback

		for(int i=LEN_TX_RX*2, j=0; i<LEN_TX_RX*4 ; i+=2, j++)
		{
			// RX
				// Posicion par LEFT: solo volteo half-words de LEFT
					h_l = buffer_rx_adc_pcm4202[i] 		&  0x0000FFFF;
					l_l = (buffer_rx_adc_pcm4202[i] >> 16) &  0x0000FFFF;

					buffer_rx_left_q31[j]  = (h_l<<16) | l_l;

				// Posicion impar RIGHT: solo volteo half-words de RIGHT y las copio en el buffer auxiliar 1
					h_r = buffer_rx_adc_pcm4202[i+1]		  &  0x0000FFFF;
					l_r = (buffer_rx_adc_pcm4202[i+1] >> 16) &  0x0000FFFF;

					buffer_rx_right_q31[j] = (h_r<<16) | l_r;

			// TX
				// Posicion par LEFT: solo volteo half-words de LEFT
					h_l = buffer_tx_left_q31[j] 		&  0x0000FFFF;
					l_l = (buffer_tx_left_q31[j] >> 16) &  0x0000FFFF;

					buffer_tx_dac_pcm1781[i]  = (h_l<<16) | l_l;

					// Posicion impar RIGHT: solo volteo half-words de RIGHT y las copio en el buffer auxiliar 2
					h_r = buffer_tx_right_q31[j] 		  &  0x0000FFFF;
					l_r = (buffer_tx_right_q31[j] >> 16) &  0x0000FFFF;

					buffer_tx_dac_pcm1781[i+1] = (h_r<<16) | l_r;
		}

		HAL_GPIO_TogglePin(EXP6_GPIO_Port, EXP6_Pin);

		transmit_ready = HAL_OK;
	}

	 void HAL_I2S_RxHalfCpltCallback(I2S_HandleTypeDef *hi2s)
	 {
	 }

	 void HAL_I2S_RxCpltCallback(I2S_HandleTypeDef *hi2s)
	 {
	 }

	  void HAL_I2S_ErrorCallback(I2S_HandleTypeDef *hi2s)
	 {
		 Error_Handler();
	 }









