/*
 * I2S.h
 *
 *  Created on: 23 mar. 2018
 *      Author: alumno
 */

#ifndef MY_I2S_PRIV_H
#define MY_I2S_PRIV_H










/*****************************************************************************************************
 * 											INFO
 *****************************************************************************************************/
	// When the master clock is generated (MCKOE in the SPI_I2SPR register is set):
	// 		FS = I2SxCLK / [(16*2)*((2*I2SDIV)+ODD)*8)] when the channel frame is 16-bit wide
	//		FS = I2SxCLK / [(32*2)*((2*I2SDIV)+ODD)*4)] when the channel frame is 32-bit wide
	// When the master clock is disabled (MCKOE bit cleared):
	//		FS = I2SxCLK / [(16*2)*((2*I2SDIV)+ODD))] when the channel frame is 16-bit wide
	//		FS = I2SxCLK / [(32*2)*((2*I2SDIV)+ODD))] when the channel frame is 32-bit wide
	// I2SDIV = SPI_I2SPR[7:0]
	// I2SODD = SPI_I2SPR[8]










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include <stm32f4xx_hal.h>
	#include <my_arm_math.h>

	#include "defines.h"










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Externas
		extern I2S_HandleTypeDef hi2s2;
		extern I2S_HandleTypeDef hi2s3;
		extern q31_t buffer_rx_adc_pcm4202[LEN_TX_RX*4];
		extern q31_t buffer_tx_dac_pcm1781[LEN_TX_RX*4];










#endif /* MY_I2S_PRIV_H */
