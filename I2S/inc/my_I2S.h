/*
 * I2S.h
 *
 *  Created on: 13 sept. 2017
 *      Author: alumno
 */

#ifndef MY_I2S_H
#define MY_I2S_H










/*****************************************************************************************************
 * 											INFO
 *****************************************************************************************************/
	// When the master clock is generated (MCKOE in the SPI_I2SPR register is set):
	// 		FS = I2SxCLK / [(16*2)*((2*I2SDIV)+ODD)*8)] when the channel frame is 16-bit wide
	//		FS = I2SxCLK / [(32*2)*((2*I2SDIV)+ODD)*4)] when the channel frame is 32-bit wide
	// When the master clock is disabled (MCKOE bit cleared):
	//		FS = I2SxCLK / [(16*2)*((2*I2SDIV)+ODD))] when the channel frame is 16-bit wide
	//		FS = I2SxCLK / [(32*2)*((2*I2SDIV)+ODD))] when the channel frame is 32-bit wide
	// I2SDIV = SPI_I2SPR[7:0]
	// I2SODD = SPI_I2SPR[8]










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "stm32f4xx_hal.h"

	#include "defines.h"










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Externas
		// RX Left
			extern float32_t *buffer_rx_left;

		// RX Right
			extern float32_t *buffer_rx_right;

		// TX Left
			extern float32_t *buffer_tx_left;

		// TX Right
			extern float32_t *buffer_tx_right;

		// Flags
			extern uint8_t transmit_ready;
			extern uint8_t recieve_ready;










/*****************************************************************************************************
 * 											FUNCIONES
 *****************************************************************************************************/
	/**
	  * @brief 	Acomoda los datos leidos del ADC en 2 canales separados (buffer_rx_left y buffer_rx_right) para poder procesar.
	  * 		Se pueden modificar esos mismos vectores.
	  * @param  hi2s: pointer to a I2S_HandleTypeDef structure that contains
	  *         the configuration information for I2S module
	  * @retval None
	  */
	void I2S_Read();

	/**
	  * @brief 	Acomoda los datos procesados en 1 canal todo junto
	  * 		(buffer_tx_left y buffer_tx_right en forma alternada) para poder enviar al DAC.
	  * @param  none
	  * @retval None
	  */
	void I2S_Write();

	/**
	  * @brief 	Obtiene la frecuencia de muestreo REAL
	  * @param  none
	  * @retval Frecuencia de muestreo del periferico I2S
	  */
	uint32_t I2S_get_fs();

	/**
	  * @brief 	Configura la frecuencia de trabajo del periferico I2S
	  * @param  none
	  * @retval None
	  */
	void I2S_init_clk();










/*****************************************************************************************************
 * 											INTERRUPCIONES
 *****************************************************************************************************/
	/**
	  * @brief Tx Transfer Half completed callbacks
	  * @param  hi2s: pointer to a I2S_HandleTypeDef structure that contains
	  *         the configuration information for I2S module
	  * @retval None
	  */
	 void HAL_I2S_TxHalfCpltCallback(I2S_HandleTypeDef *hi2s);

	 /**
	  * @brief Tx Transfer completed callbacks
	  * @param  hi2s: pointer to a I2S_HandleTypeDef structure that contains
	  *         the configuration information for I2S module
	  * @retval None
	  */
	 void HAL_I2S_TxCpltCallback(I2S_HandleTypeDef *hi2s);

	 /**
	   * @brief Rx Transfer half completed callbacks
	   * @param  hi2s: pointer to a I2S_HandleTypeDef structure that contains
	   *         the configuration information for I2S module
	   * @retval None
	   */
	 void HAL_I2S_RxHalfCpltCallback(I2S_HandleTypeDef *hi2s);

	 /**
	   * @brief Rx Transfer completed callbacks
	   * @param  hi2s: pointer to a I2S_HandleTypeDef structure that contains
	   *         the configuration information for I2S module
	   * @retval None
	   */

	 void HAL_I2S_RxCpltCallback(I2S_HandleTypeDef *hi2s);

	 /**
	   * @brief I2S error callbacks
	   * @param  hi2s: pointer to a I2S_HandleTypeDef structure that contains
	   *         the configuration information for I2S module
	   * @retval None
	   */
	  void HAL_I2S_ErrorCallback(I2S_HandleTypeDef *hi2s);










#endif /* MY_I2S_H */
