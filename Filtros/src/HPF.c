/*
 * LPFNormaFM.c
 *
 *  Created on: 13 mar. 2018
 *      Author: alumno
 */










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "HPF.h"

	#include "header.h"
	#include "defines.h"










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Globales
		arm_biquad_casd_df1_inst_f32 hpf;

	// Locales
		static float32_t pState_hpf[4*NUM_SECTIONS_LPF_NOMRA_FM];
		static const float32_t pCoeffs[]={	8.67651761E-01, -1.73530352E+00, 8.67651761E-01, 1.61409533E+00, -8.56511772E-01,
										7.63465226E-01, -1.52693045E+00, 7.63465226E-01, 1.42027676E+00, -6.33584082E-01,
										6.92708433E-01, -1.38541687E+00, 6.92708433E-01, 1.28864765E+00, -4.82186019E-01,
										6.49007559E-01, -1.29801512E+00, 6.49007559E-01, 1.20735085E+00, -3.88679504E-01,
										6.28173113E-01, -1.25634623E+00, 6.28173113E-01, 1.16859245E+00, -3.44100058E-01};










/*****************************************************************************************************
 * 											FUNCI�N INIT
 *****************************************************************************************************/
void f_filtro_HPF_init()
{
	arm_biquad_cascade_df1_init_f32(&hpf, NUM_SECTIONS_HPF, (float32_t*) pCoeffs, pState_hpf);
}

void f_filtro_HPF_do()
{
	HAL_GPIO_TogglePin(EXP2_GPIO_Port, EXP2_Pin);
	arm_biquad_cascade_df1_f32(&hpf,buffer_rx_left,buffer_rx_left,LEN_TX_RX);
	arm_biquad_cascade_df1_f32(&hpf,buffer_rx_right,buffer_rx_right,LEN_TX_RX);
	HAL_GPIO_TogglePin(EXP2_GPIO_Port, EXP2_Pin);
}
