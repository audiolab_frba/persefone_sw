/*
 * LPFNormaFM.c
 *
 *  Created on: 13 mar. 2018
 *      Author: alumno
 */










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "LPFNormaFM.h"

	#include "header.h"
	#include "defines.h"










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Globales
		arm_biquad_casd_df1_inst_f32 lpf_n2;

	// Locales
		static float32_t pState_lpf_n2[4*NUM_SECTIONS_LPF_NOMRA_FM];
		static const float32_t pCoeffs[]={	1.53164104E-01, 3.06328207E-01, 1.53164104E-01, 1.27802527E+00, -8.90681684E-01,
										1.38238207E-01, 2.76476413E-01, 1.38238207E-01, 1.15348113E+00, -7.06433952E-01,
										1.26379251E-01, 2.52758503E-01, 1.26379251E-01, 1.05452824E+00, -5.60045302E-01,
										1.16977781E-01, 2.33955562E-01, 1.16977781E-01, 9.76080894E-01, -4.43992019E-01,
										1.09576382E-01, 2.19152763E-01, 1.09576382E-01, 9.14322436E-01, -3.52627993E-01,
										1.03834189E-01, 2.07668379E-01, 1.03834189E-01, 8.66408646E-01, -2.81745404E-01,
										9.94999036E-02, 1.98999807E-01, 9.94999036E-02, 8.30242693E-01, -2.28242323E-01,
										9.63921472E-02, 1.92784294E-01, 9.63921472E-02, 8.04311097E-01, -1.89879686E-01,
										9.43855271E-02, 1.88771054E-01, 9.43855271E-02, 7.87567556E-01, -1.65109649E-01,
										9.34012309E-02, 1.86802462E-01, 9.34012309E-02, 7.79354453E-01, -1.52959332E-01};


/*****************************************************************************************************
 * 											FUNCI�N INIT
 *****************************************************************************************************/
void f_filtro_LPFNormaFM_init()
{
	arm_biquad_cascade_df1_init_f32(&lpf_n2, NUM_SECTIONS_LPF_NOMRA_FM, (float32_t*) pCoeffs, pState_lpf_n2);
}

void f_filtro_LPFNormaFM_do()
{
	HAL_GPIO_TogglePin(EXP2_GPIO_Port, EXP2_Pin);

	arm_biquad_cascade_df1_f32(&lpf_n2,buffer_rx_left,buffer_tx_left,LEN_TX_RX);
	arm_biquad_cascade_df1_f32(&lpf_n2,buffer_rx_right,buffer_tx_right,LEN_TX_RX);

	arm_copy_f32(buffer_tx_left,buffer_rx_left,LEN_TX_RX);
	arm_copy_f32(buffer_tx_right,buffer_rx_right,LEN_TX_RX);

	HAL_GPIO_TogglePin(EXP2_GPIO_Port, EXP2_Pin);
}

void f_filtro_LPFNormaFM_do2()
{
	HAL_GPIO_TogglePin(EXP2_GPIO_Port, EXP2_Pin);

	arm_biquad_cascade_df1_f32(&lpf_n2,buffer_rx_left,buffer_tx_left,LEN_TX_RX);
	arm_biquad_cascade_df1_f32(&lpf_n2,buffer_rx_right,buffer_tx_right,LEN_TX_RX);

	arm_copy_f32(buffer_tx_left,buffer_rx_left,LEN_TX_RX);
	arm_copy_f32(buffer_tx_right,buffer_rx_right,LEN_TX_RX);

	HAL_GPIO_TogglePin(EXP2_GPIO_Port, EXP2_Pin);
}
