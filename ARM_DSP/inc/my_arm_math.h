/*
 * my_arm_math.h
 *
 *  Created on: 8 mar. 2018
 *      Author: alumno
 */

#ifndef MY_ARM_MATH_H
#define MY_ARM_MATH_H










/*****************************************************************************************************
 * 											DEFINES
 *****************************************************************************************************/
	#define ARM_MATH_CM4
	#define __FPU_PRESENT 	1U
	#define ARM_MATH_DSP










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include <stm32f4xx_hal.h>

	#include "arm_math.h"
	#include "arm_common_tables.h"










/*****************************************************************************************************
 * 											FUNCIONES
 *****************************************************************************************************/
	/**
	  * @brief 		Devuelve siguiente valor del tono de 19kHz (por tabla). Cada vez que la funcion es llamada, devuelve un valor distinto
	  * @param[in] 	none
	  * @retval 	Valor del tono de 19kHz
	  */
	float32_t arm_tono_19k_use();

	/**
	  * @brief		Devuelve siguiente valor del tono de 38kHz (por tabla). Cada vez que la funcion es llamada, devuelve un valor distinto
	  * @param[in] 	none
	  * @retval 	Valor del tono de 38kHz
	  */
	float32_t arm_tono_38k_use();










#endif /* MY_ARM_MATH_H */
