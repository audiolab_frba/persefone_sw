/*
 * pulsadores_loop.c
 *
 *  Created on: 9 mar. 2018
 *      Author: alumno
 */










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "pulsadores.h"

	#include "header.h"










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Globales
		uint8_t pulsador_1;
		uint8_t pulsador_2;
		uint8_t pulsador_3;
		uint8_t pulsador_4;
		uint8_t sel = SEL_LyR;










/*****************************************************************************************************
 * 											FUNCIONES
 *****************************************************************************************************/
		void Pulsadores_read()
		{
			pulsador_1 = !HAL_GPIO_ReadPin(SW1_GPIO_Port, SW1_Pin);			// cambiar SW1 por SW2 estan invertidos
			pulsador_2 = !HAL_GPIO_ReadPin(SW2_GPIO_Port, SW2_Pin);
			pulsador_3 = !HAL_GPIO_ReadPin(SW3_GPIO_Port, SW3_Pin);
			pulsador_4 = !HAL_GPIO_ReadPin(SW4_GPIO_Port, SW4_Pin);
		}

		void Pulsadores_MPX_scale()
		{
			if(pulsador_1)
			{
				sel--;
				sel = (sel + SEL_MAX) % SEL_MAX;
			}
			if(pulsador_2)
			{
				sel++;
				sel %= SEL_MAX;
			}

			switch(sel)
			{
				case SEL_LyR:
					scale_l_r += pulsador_4*PASO;
					scale_l_r -= pulsador_3*PASO;
					break;
				case SEL_P19K:
					scale_piloto19k += pulsador_4*PASO;
					scale_piloto19k -= pulsador_3*PASO;
					break;
				case SEL_P38K:
					scale_piloto38k += pulsador_4*PASO;
					scale_piloto38k -= pulsador_3*PASO;
					break;
				case SEL_MPX:
					scale_mpx += pulsador_4*PASO;
					scale_mpx -= pulsador_3*PASO;
					break;
			}
		}









