/*
 * pulsadores.h
 *
 *  Created on: 9 mar. 2018
 *      Author: alumno
 */

#ifndef PULSADORES_H
#define PULSADORES_H










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "stm32f4xx_hal.h"










/*****************************************************************************************************
 * 											DEFINES
 *****************************************************************************************************/
	#define	SEL_MAX		4
	#define	SEL_MIN		0

	#define	SEL_LyR		0
	#define	SEL_P19K	1
	#define	SEL_P38K	2
	#define	SEL_MPX		3

	#define	PASO		0.05









/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Externas
		extern uint8_t pulsador_1;
		extern uint8_t pulsador_2;
		extern uint8_t pulsador_3;
		extern uint8_t pulsador_4;
		extern uint8_t sel;










/*****************************************************************************************************
 * 											FUNCIONES
 *****************************************************************************************************/
	/**
	  * @brief 	Lee los pulsadores de la Persefone
	  * @param  None
	  * @retval None
	  */
	void Pulsadores_read();

	/**
		  * @brief 	En funcion de los pulsadores, realiza las escalas correspondientes en la senial MPXs
		  * @param  None
		  * @retval None
	*/
	void Pulsadores_MPX_scale();









#endif /* PULSADORES_H */
