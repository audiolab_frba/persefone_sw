/*
 * init.c
 *
 *  Created on: 22 mar. 2018
 *      Author: pablo
 */










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "header.h"










/*****************************************************************************************************
 * 											INIT
 *****************************************************************************************************/
	void init_main()
	{
		// Prende LED rojo
			HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_SET);
			HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);

		// Set I2S clk
			I2S_init_clk();

		// Initialize Display ILI9341
			TM_ILI9341_Init();
			TM_ILI9341_Welcome_Screen();
			XPT_2046_Init();

		// Initialize ADC and DAC
			ADC_PCM4202_init();
			DAC_PCM1781_init();

		// Launch conversion
			ADC_PCM4202_recieve();
			DAC_PCM1781_transmit();

		// Setea valor a potenciometros SPI
			//  uint8_t value_pot=127;
			//  SPI_Pote1Val(value_pot);
			//  SPI_Pote2Val(value_pot);

		// Inicializa filtros digitales
			f_filtro_LPFNormaFM_init();
			f_filtro_HPF_init();


		// Apaga LED rojo
			HAL_GPIO_WritePin(LED_R_GPIO_Port, LED_R_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(LED_G_GPIO_Port, LED_G_Pin, GPIO_PIN_RESET);
			HAL_GPIO_WritePin(LED_B_GPIO_Port, LED_B_Pin, GPIO_PIN_RESET);
	}
