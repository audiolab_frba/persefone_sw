/*
 * mpx.c
 *
 *  Created on: 7 mar. 2018
 *      Author: alumno
 */










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "header.h"
	#include "defines.h"










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Externas
	// Internas
	extern float32_t buffer_19k[LEN_TX_RX];
	extern float32_t buffer_38k[LEN_TX_RX];









/*****************************************************************************************************
 * 											FUNCIONES
 *****************************************************************************************************/
	/**
	  * @brief 	Esta funcion presupone que la se�al L y R fueron filtradas de modo que a
	  * 19k haya una atenuaci�n de 45dB. Segun norma FM Argentina.
	  * @param  None
	  * @retval None
	  */
	void mpx_loop()
	{
		float32_t *buffer_suma = buffer_aux1_f32;
		float32_t *buffer_resta = buffer_aux2_f32;
		float32_t *buffer_38kxl_r = buffer_aux2_f32;
		float32_t *buffer_mpx = buffer_aux1_f32;


		HAL_GPIO_TogglePin(EXP4_GPIO_Port, EXP4_Pin);


		// Escalo entradas L y R
			arm_scale_f32(buffer_rx_left, scale_l_r, buffer_rx_left, LEN_TX_RX);
			arm_scale_f32(buffer_rx_right, scale_l_r, buffer_rx_right, LEN_TX_RX);

		// buffer_suma = L + R
			arm_add_f32(buffer_rx_left, buffer_rx_right, buffer_suma, LEN_TX_RX);	// suma = buffer 1

		// buffer_resta = L - R
			arm_negate_f32(buffer_rx_right, buffer_rx_right, LEN_TX_RX);
			arm_add_f32(buffer_rx_left, buffer_rx_right, buffer_resta, LEN_TX_RX);	// resta = buffer 2

		// genero 19k y 38k
		  for(int i=0; i<LEN_TX_RX; i++)
		  {
			  buffer_19k[i] = arm_tono_19k_use();
			  buffer_38k[i] = arm_tono_38k_use();
		  }
		  arm_scale_f32(buffer_19k, scale_piloto19k, buffer_19k, LEN_TX_RX);
		  arm_scale_f32(buffer_38k, scale_piloto38k, buffer_38k, LEN_TX_RX);

		// buffer_38kxl_r = 38k x resta
			arm_mult_f32(buffer_38k, buffer_resta, buffer_38kxl_r, LEN_TX_RX);		// 38kxl_r = buffer 2

		// mpx = L+R + 19K + buffer_38kxl_r
			arm_add_f32(buffer_suma, buffer_19k, buffer_mpx, LEN_TX_RX);			// mpx = buffer 1
			arm_add_f32(buffer_38kxl_r, buffer_mpx, buffer_mpx, LEN_TX_RX);			// mpx = buffer 1

			arm_copy_f32(buffer_mpx,buffer_tx_left,LEN_TX_RX);
			arm_copy_f32(buffer_mpx,buffer_tx_right,LEN_TX_RX);

			arm_scale_f32(buffer_tx_left, scale_mpx, buffer_tx_left, LEN_TX_RX);
			arm_scale_f32(buffer_tx_right, scale_mpx, buffer_tx_right, LEN_TX_RX);

		HAL_GPIO_TogglePin(EXP4_GPIO_Port, EXP4_Pin);
	}

	/**
	  * @brief 	Funcion para medir tiempos de procesamiento con distintos GPIOs.
	  * @param  None
	  * @retval None
	  */
	void mpx_measure_times()
	{
		static uint8_t estado = 1;

		if(transmit_ready == HAL_OK)
		{
			HAL_GPIO_TogglePin(EXP0_GPIO_Port, EXP0_Pin);

			HAL_GPIO_TogglePin(EXP1_GPIO_Port, EXP1_Pin);
			I2S_Read();
			HAL_GPIO_TogglePin(EXP1_GPIO_Port, EXP1_Pin);

			// Comienzo a procesar se�al

				HAL_GPIO_TogglePin(EXP3_GPIO_Port, EXP3_Pin);
				Pulsadores_read();
				ADC_interno_read();

				if(pulsador_1)
				{
					estado = 1;
				}
				else if(pulsador_2)
				{
					estado = 2;
				}
				else if(pulsador_3)
				{
					estado = 3;
				}
				else if(pulsador_4)
				{
					estado = 4;
				}

				if(estado == 1)
				{
					mpx_loop();
				}
				else if(estado == 2)
				{
					f_filtro_LPFNormaFM_do();
					arm_copy_f32(buffer_rx_left, buffer_tx_left, LEN_TX_RX);
					arm_copy_f32(buffer_rx_right, buffer_tx_right, LEN_TX_RX);
				}
				else if(estado == 3)
				{
					//f_filtro_LPFNormaFM_do2();
					arm_copy_f32(buffer_rx_left, buffer_tx_left, LEN_TX_RX);
					arm_copy_f32(buffer_rx_right, buffer_tx_right, LEN_TX_RX);
				}
				else if(estado == 4)
				{
					f_filtro_LPFNormaFM_do();
					mpx_loop();
				}

				HAL_GPIO_TogglePin(EXP3_GPIO_Port, EXP3_Pin);

			// Ya no puedo procesar se�al

			HAL_GPIO_TogglePin(EXP5_GPIO_Port, EXP5_Pin);
			I2S_Write();
			HAL_GPIO_TogglePin(EXP5_GPIO_Port, EXP5_Pin);

			HAL_GPIO_TogglePin(EXP0_GPIO_Port, EXP0_Pin);
		}
	}








/*****************************************************************************************************
 * 											INTERRUPCIONES
 *****************************************************************************************************/
	/**
	  * @brief 	None
	  * @param  None
	  * @retval None
	  */

