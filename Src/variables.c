/*
 * variables.c
 *
 *  Created on: 22 mar. 2018
 *      Author: pablo
 */










/*****************************************************************************************************
 * 											INCLUDES
 *****************************************************************************************************/
	#include "defines.h"
	#include "header.h"










/*****************************************************************************************************
 * 											VARIABLES
 *****************************************************************************************************/
	// Globales
		uint32_t buffer_aux1[LEN_TX_RX];
		uint32_t buffer_aux2[LEN_TX_RX];

		uint32_t*	buffer_aux1_u32 = (uint32_t*) buffer_aux1;
		q15_t*		buffer_aux1_q15 = (q15_t*) buffer_aux1;
		q31_t*		buffer_aux1_q31 = (q31_t*) buffer_aux1;
		float32_t*	buffer_aux1_f32 = (float32_t*) buffer_aux1;

		uint32_t*	buffer_aux2_u32 = (uint32_t*) buffer_aux2;
		q15_t*		buffer_aux2_q15 = (q15_t*) buffer_aux2;
		q31_t*		buffer_aux2_q31 = (q31_t*) buffer_aux2;
		float32_t*	buffer_aux2_f32 = (float32_t*) buffer_aux2;

		float32_t scale_l_r = 0.35;
		float32_t scale_piloto19k = 0.8;
		float32_t scale_piloto38k = 0.2;
		float32_t scale_mpx = 0.5;






