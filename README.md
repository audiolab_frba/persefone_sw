# Proyecto AudioLab #

Placa de desarrollo basada en el microcontrolador STM32F407zgt6

### caracteristicas del micro: ###

* 32 bits
* unidad de punto flotante
* instrucciones DSP

### caracteristicas de la placa ###

* adc y dac de 24 bits hasta 192kHz 
* entradas de audio tipo plug
* conexi�n a TFT
* controles ajustables por potenciometros

### notas de uso ###

* Las funciones   de cmsis deben incluir los .h:
     stm32f4xx_hal.h
	 headers.h